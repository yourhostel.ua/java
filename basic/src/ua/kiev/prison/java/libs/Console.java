package libs;

import java.util.Scanner;

public class Console {
    public static void log(String line) {
        System.out.println(line);
    }
    public static String nextLine() {
        return new Scanner(System.in).nextLine();
    }

    public static String typeOf(int a){return "int";}
    public static String typeOf(Integer a){return "Integer";}
    public static String typeOf(boolean a){return "boolean";}
    public static String typeOf(Boolean a){return "Boolean";}
    public static String typeOf(long a){return "long";}
    public static String typeOf(Long a){return "Long";}
    public static String typeOf(char a){return "char";}
    public static String typeOf(Character a){return "Character";}
    public static String typeOf(short a){return "short";}
    public static String typeOf(Short a){return "Short";}
    public static String typeOf(String a){return "String";}
}

