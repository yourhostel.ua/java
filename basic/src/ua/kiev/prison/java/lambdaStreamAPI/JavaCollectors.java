package lambdaStreamAPI;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JavaCollectors {
    public static void main(String[] args) {
        System.out.println("\n.toList()------------------------------------------------------------------------------");
        List<Integer> list = Stream.of(1, 2, 3)
                .collect(Collectors.toList());//toSet(), toMap()
        System.out.println(list);//[1, 2, 3]
        System.out.println("\n.joining(\"-\", \"<\", \">\")----------------------------------------------------------");
        String s1 = Stream.of(1, 2, 3)
                .map(String::valueOf)
                .collect(Collectors.joining("-", "<", ">"));
        System.out.println(s1); //"<1-2-3>"

        List<String> phones1 = new ArrayList<>();
        Collections.addAll(phones1, "iPhone 8", "HTC U12", "Huawei Nexus 6P",
                "Samsung Galaxy S9", "LG G6", "Xiaomi MI6", "ASUS Zenfone 2",
                "Sony Xperia Z5", "Meizu Pro 6", "Lenovo S850");

//        List<String> filteredPhones = phones.stream()
//                .filter(s->s.length()<10)
//                .collect(Collectors.toList());

        System.out.println("\n.toSet()-------------------------------------------------------------------------------");
        Set<String> filteredPhones = phones1.stream()
                .filter(s->s.length()<10)
                .collect(Collectors.toSet());
        System.out.println(filteredPhones);//[iPhone 8, HTC U12, LG G6]
        System.out.println("\n.toMap()-------------------------------------------------------------------------------");
        Stream<Phone> phoneStream = Stream.of(new Phone("iPhone 8", 54000),
                new Phone("Nokia 9", 45000),
                new Phone("Samsung Galaxy S9", 40000),
                new Phone("LG G6", 32000));

        Map<String, Integer> phones2 = phoneStream
                .collect(Collectors.toMap(p->p.getName(), t->t.getPrice()));
        System.out.println(phones2);
        phones2.forEach((k,v)->System.out.println(k + " " + v));
        System.out.println("\nHashSet::new---------------------------------------------------------------------------");
        Stream<String> phones3 = Stream.of("iPhone 8", "HTC U12", "Huawei Nexus 6P",
                "Samsung Galaxy S9", "LG G6", "Xiaomi MI6", "ASUS Zenfone 2",
                "Sony Xperia Z5", "Meizu Pro 6", "Lenovo S850");

        HashSet<String> filteredPhones2 = phones3.filter(s->s.length()<12).
                collect(Collectors.toCollection(HashSet::new));

        filteredPhones2.forEach(s->System.out.println(s));
    }
    static class Phone{

        private String name;
        private int price;

        public Phone(String name, int price){
            this.name=name;
            this.price=price;
        }

        public String getName() { return name; }
        public int getPrice() { return price; }
    }
}
