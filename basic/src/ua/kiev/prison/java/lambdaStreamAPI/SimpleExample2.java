package lambdaStreamAPI;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;


public class SimpleExample2 {
    public static void main(String[] args) {
        Stream s1 = Stream.empty();
        List<String> list = new ArrayList<>();
        Stream<String> s2 = list.stream();
        HashMap<String, String> map = new HashMap<>();
        Stream<Map.Entry<String, String>> s3 = map.entrySet().stream();
        Stream<String> s4 = Arrays.stream(new String[]{});
        Stream<String> s5 = Stream.of("a", "b", "c");
        System.out.println(s4);
        System.out.println(Integer.parseInt("10", 16));

        Object[] a = Stream.of(1, 2, 3, 4, 5, 6)
                .flatMap(x -> switch (x % 3) {
                    case 0 -> Stream.of(x, x * x, x * x * 2);
                    case 1 -> Stream.of(x);
                    case 2 -> Stream.empty();
                    default -> throw new IllegalStateException("Unexpected value: " + x % 3);
                })
                .toArray();
        System.out.println(Arrays.toString(a));//[1, 3, 9, 18, 4, 6, 36, 72]

        Object[] b = Stream.of(1, 2, 3, 4, 5, 6)
                .flatMap(x -> {
                    if (x % 2 == 0) {
                        return Stream.of(-x, x);
                    }
                    return Stream.empty();
                })
                .toArray();
        System.out.println(Arrays.toString(b));//[-2, 2, -4, 4, -6, 6]

        Object[] c = Stream.of(1, 2, 3, 4, 5, 6)
                .mapMulti((x, consumer) -> {
                    if (x % 2 == 0) {
                        consumer.accept(-x);
                        consumer.accept(x);
                    }
                })
                .toArray();
        System.out.println(Arrays.toString(c));//[-2, 2, -4, 4, -6, 6]

        System.out.println(Arrays.toString(IntStream.of(1, 3, 2, 0, 5, 4)
                .dropWhile(x -> x % 2 == 1)
                .toArray()));//[2, 0, 5, 4]

        Optional<Integer> result = Stream.<Integer>empty()
                .reduce((acc, x) -> acc + x);
        System.out.println(result.isPresent());//false

        Optional<Integer> sum = Stream.of(1, 2, 3, 4, 5)
                .reduce((acc, x) -> acc + x);
        System.out.println(sum.get());

        System.out.println(IntStream.of(2, 4, 6, 8)
                .reduce(6, (acc, x) -> acc + x));//Integer::sum

        System.out.printf("min: %d\n", Stream.of(20, 11, 45, 78, 13)
                .min(Integer::compare).get());//11

        System.out.printf("max: %d\n", Stream.of(20, 11, 45, 78, 13)
                .max(Integer::compare).get());//78

        System.out.printf("findAny as findFirst: %d\n", IntStream.range(4, 65536)
                .findAny()
                .getAsInt());//4

        System.out.printf("findFirst: %d\n", IntStream.range(4, 65536)
                .findFirst()
                .getAsInt());//4

        System.out.printf("parallel + findAny: %d\n", IntStream.range(4, 65536)
                .parallel()
                .findAny()
                .getAsInt());//43009

        System.out.printf("parallel + findFirst: %d\n", IntStream.range(4, 65536)
                .parallel()
                .findFirst()
                .getAsInt());//4

        System.out.println(Stream.of(1, 2, 3, 4, 5)
                .allMatch(x -> x <= 7));//true

        System.out.println(Stream.of(1, 2, 3, 4, 5)
                .anyMatch(x -> x == 3));//true

        //только для примитивов
        LongSummaryStatistics stats = LongStream.range(2, 16)
                .summaryStatistics();
        System.out.println(Arrays.toString(LongStream.range(2, 16).toArray()));
        System.out.format("  count: %d%n", stats.getCount());//count: 14
        System.out.format("    sum: %d%n", stats.getSum());//sum: 119
        System.out.format("average: %.1f%n", stats.getAverage());//average: 8,5
        System.out.format("    min: %d%n", stats.getMin());//min: 2
        System.out.format("    max: %d%n", stats.getMax());//max: 15

        Runnable r = new Runnable(){
            @Override
            public void run() {
                System.out.println("Что-то делаем в методе run()");
            }};

        Runnable r1 = () -> {
            System.out.println("Что-то делаем в методе () ->");
        };
        r.run();
        r1.run();
    }
}
