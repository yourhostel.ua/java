package lambdaStreamAPI;

import java.util.Arrays;
import java.util.stream.IntStream;

public class SimpleExample1 {
    public static void main(String[] args) {
        System.out.println("\n.filter----------------------------------------------------------------------------------");
        System.out.println(Arrays.toString(filteredToArray(7, 12, 4, 666, 1)));
        System.out.println("\n.map-------------------------------------------------------------------------------------");
        System.out.println(Arrays.toString(mappedToArray(12, 666, 1)));
        System.out.println("\n.flatMap---------------------------------------------------------------------------------");
        System.out.println(Arrays.toString(flatMappedToArray(2, 5)));
        System.out.println("\n.forEach---------------------------------------------------------------------------------");
        IntStream.of(7, 666, 1)
                .forEach(System.out::println);
    }

    public static int[] filteredToArray(int... a) {
        return IntStream.of(a)
                .filter(x -> x < 10) // оставляем только элементы меньше 10
                .toArray();
    }

    public static int[] mappedToArray(int... a){
        return IntStream.of(a)
                .map(x -> x + 7) // добавляем каждого элементу прошедшему фильтр 7
                .toArray();
    }

    public static int[] flatMappedToArray(int... a){
        return IntStream.of(a)
                .flatMap(x -> IntStream.range(0,x)) // [0, 1, 0, 1, 2, 3, 4]
                .toArray();
    }
}
