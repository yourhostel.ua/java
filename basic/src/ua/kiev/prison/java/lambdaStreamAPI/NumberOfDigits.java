package lambdaStreamAPI;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class NumberOfDigits {
    public static Map<Character, Long> MapCharToAmount(int min, int max) {
        return IntStream
                .rangeClosed(min, max)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining())
                .codePoints()
                .mapToObj(value -> (char) value)
                .collect(Collectors.groupingBy( v -> v,Collectors.counting()
        ));
    }

    static final IntStream data = IntStream.rangeClosed(173, 215);

    public static void main(String[] args) {
        Map<String, Long> test = Arrays.stream(data
                        .mapToObj(String::valueOf)
                        .collect(Collectors.joining())
                        .split(""))
                .collect(Collectors.groupingBy(i -> i, Collectors.counting()));
        System.out.println(test);

        Map<Character, Long> characterIntegerMap = MapCharToAmount(173, 215);
        System.out.println(characterIntegerMap);
    }
}
