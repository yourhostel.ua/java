package type;

public class Characters {
    public static void main(String[] args) {
        char a = 'a', b, c = 'c';
        b = (char) ((a + c) / 2); // Можно складывать, вычитать, делить и умножать
                 // Но из-за особенностей арифметики Java результат приходится приводить к типу char явно
        System.out.println(b); // Выведет символ 'b'
    }
}
