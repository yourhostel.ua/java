package type;

public class BooleanType {
    public static void main(String[] args) {
        boolean a, b = false, c = true;

        a = b == c;

        System.out.println(a);
    }
}
/*
 Тип	                Размер (бит)	                               Значение
 boolean	8 (в массивах), 32 (не в массивах используется int)	true (истина) или false (ложь)
 */