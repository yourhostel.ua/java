package type;

public class IntegralTypes {
    public static void main(String[] args) {
        byte b = 115;
        short s = 1123;
        int i = 64536;
        long l = 2147483648L; // Постфикс l или L обозначает литералы типа long
        System.out.println(i);
        System.out.println(b);
        System.out.println(s);
        System.out.println(l);
    }
}

/*
 * Тип	  Размер (бит)	      Диапазон
 * byte	    8 бит	          от -128 до 127
 * short	16 бит	          от -32768 до 32767
 * char	    16 бит	          беззнаковое целое число, представляющее собой символ UTF-16 (буквы и цифры)
 * int	    32 бит	          от -2147483648 до 2147483647 //2^31
 * long	    64 бит	          от -9223372036854775808L до 9223372036854775807L //2^61
 */
