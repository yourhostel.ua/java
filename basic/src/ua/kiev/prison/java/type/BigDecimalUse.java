package type;

import java.math.BigDecimal;
public class BigDecimalUse {
    public static void main(String[] args) {

                double total = 0.2;
                for (int i = 0; i < 100; i++) {
                    total += 0.2;
                }
                System.out.println("total = " + total);//похибка під час використання double
//замість 20.2 видасть total = 20.19999999999996

//для розрахунку валюти можна використовувати приведення через BigDecimal
        int scale = 4;
        double value = 0.11111;
        BigDecimal tempBig = new BigDecimal(Double.toString(value));
        tempBig = tempBig.setScale(scale, BigDecimal.ROUND_HALF_EVEN);
        String strValue = tempBig.stripTrailingZeros().toPlainString();
        System.out.println("tempBig = " + strValue);
        //tempBig = 0.1111

    }
}
