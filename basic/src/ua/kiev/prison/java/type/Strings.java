package type;

public class Strings {
    public static void main(String[] args) {
        String a = "Hello", b = "World";
        System.out.println(a + " " + b); // Здесь + означает объединение (конкатенацию) строк
        // Пробел не вставляется автоматически
        // Строки конкатенируются слева направо, надо помнить это когда соединяешь строку и примитив
        String c = 2 + 2 + ""; // "4"
        String d = "" + 2 + 2; // "22"
        d = "" + (2 + 2); // а теперь d тоже "4"

        String foo = "a string";
        String bar = "a string"; // bar будет указывать на тот же объект что и foo
        String baz = new String("a string"); // Чтобы гарантированно создать новую строку надо вызвать конструктор
        System.out.println("сравнивает ссылки foo == bar ? " + (foo == bar)); // == сравнивает ссылки на объекты
        System.out.println("equals foo равен bar ? " + (foo.equals(bar))); // Метод equals служит для проверки двух объектов на равенство
        System.out.println("сравнивает ссылки foo == baz ? " + (foo == baz));
        System.out.println("equals foo равен baz ? " + (foo.equals(baz)));
    }
}
