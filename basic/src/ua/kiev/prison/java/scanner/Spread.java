package scanner;

import java.util.Arrays;
import java.util.stream.Stream;

import static libs.Console.log;
import static libs.Console.nextLine;
public class Spread {
    public static void main(String... args) {
        String a = nextLine();
        String b = nextLine();
        String c = nextLine();

        test(a, b, c);

        printMany("one", "two", "three");
        printMany(new String[]{"bum", "bah", "bams"});
        printMany(Stream.of("1", "2", "3").toArray(String[]::new));
        printMany(Arrays.asList("foo", "bar", "baz").toArray(new String[3]));

    }

    static void test(String... a) {
        for (String b : a) {
            log(String.format("%s", b));
        }
    }

    static void printMany(String ...elements) {
        Arrays.stream(elements).forEach(System.out::println);
    }
}
