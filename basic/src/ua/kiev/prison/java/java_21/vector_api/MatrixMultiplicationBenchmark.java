package java_21.vector_api;

import jdk.incubator.vector.IntVector;
import jdk.incubator.vector.VectorOperators;
import jdk.incubator.vector.VectorSpecies;

/**
 * Vector API - це частина JDK, яка знаходиться на стадії інкубатора,
 * і призначена для забезпечення високопродуктивних векторних обчислень
 * в Java-додатках на апаратному рівні. API дозволяє ефективно використовувати
 * векторні інструкції сучасних процесорів, таких як SSE та AVX в Intel і AMD,
 * для паралельної обробки даних. Використання векторних інструкцій дозволяє
 * значно підвищити продуктивність обчислень, особливо при роботі з великими
 * масивами даних.
 * Для запуску класу MatrixMultiplicationBenchmark необхідно включити інкубаторний модуль,
 * використовуючи VM опції: "--add-modules=jdk.incubator.vector" або
 * "--enable-preview --add-modules=jdk.incubator.vector".
 * У цьому прикладі демонструється використання Vector API для множення двох
 * матриць. Тестування на процесорі AMD FX-8350 з набором інструкцій MMX(+), SSE, SSE2,
 * SSE3, SSE4.1, SSE4.2, SSE4A, x86-64, AMD-V, AES, AVX, XOP, FMA3, FMA4,
 * показало кращу продуктивність при використанні SPECIES_128 для векторних обчислень з
 * результатом SIZE = 1024:
 * Звичайне множення матриць: 0,11442 хвилин
 * Векторне множення матриць: 0,06666 хвилин
 */

public class MatrixMultiplicationBenchmark {

    private static final int SIZE = 1024; // Розмір матриці

    public static void main(String[] args) {
        int[][] matrixA = generateMatrix();
        int[][] matrixB = generateMatrix();
        int[][] resultMatrix = new int[SIZE][SIZE];

        long startTime = System.nanoTime();
        regularMatrixMultiplication(matrixA, matrixB, resultMatrix);
        long endTime = System.nanoTime();
        double timeInMinutes = (endTime - startTime) / 1_000_000_000.0 / 60.0;
        System.out.println("Звичайне множення матриць: "
                + String.format("%.5f", timeInMinutes) + " хвилин");

        resultMatrix = new int[SIZE][SIZE]; // Очищення результату

        startTime = System.nanoTime();
        vectorMatrixMultiplication(matrixA, matrixB, resultMatrix);
        endTime = System.nanoTime();
        timeInMinutes = (endTime - startTime) / 1_000_000_000.0 / 60.0;
        System.out.println("Векторне множення матриць: "
                + String.format("%.5f", timeInMinutes) + " хвилин");
    }

    private static void regularMatrixMultiplication(int[][] a, int[][] b, int[][] result) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                for (int k = 0; k < SIZE; k++) {
                    result[i][j] += a[i][k] * b[k][j];
                }
            }
        }
    }

    private static void vectorMatrixMultiplication(int[][] a, int[][] b, int[][] result) {
        VectorSpecies<Integer> species = IntVector.SPECIES_128;
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                IntVector vResult = IntVector.zero(species);
                for (int k = 0; k < SIZE; k += species.length()) {
                    var vA = IntVector.fromArray(species, a[i], k);
                    var vB = IntVector.fromArray(species, b[j], k);
                    vResult = vResult.add(vA.mul(vB));
                }
                result[i][j] = vResult.reduceLanes(VectorOperators.ADD);
            }
        }
    }

    private static int[][] generateMatrix() {
        int[][] matrix = new int[MatrixMultiplicationBenchmark.SIZE][MatrixMultiplicationBenchmark.SIZE];
        for (int i = 0; i < MatrixMultiplicationBenchmark.SIZE; i++) {
            for (int j = 0; j < MatrixMultiplicationBenchmark.SIZE; j++) {
                matrix[i][j] = (int) (Math.random() * 100);
            }
        }
        return matrix;
    }

}





