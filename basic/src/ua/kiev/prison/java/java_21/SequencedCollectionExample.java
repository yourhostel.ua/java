package java_21;

import java.util.ArrayList;

public class SequencedCollectionExample {

    public static void main(String[] args) {
        ArrayList<String> languages = new ArrayList<>();

        languages.add("English");
        languages.add("Spanish");
        languages.add("French");
        languages.add("Italian");
        languages.addFirst("Portuguese");
        System.out.println(languages); //[Portuguese, English, Spanish, French, Italian]
        System.out.println(languages.reversed()); //[Italian, French, Spanish, English, Portuguese]
        System.out.println(languages); //[Portuguese, English, Spanish, French, Italian]
        languages.addLast("Ukraine");
        System.out.println(languages); //[Portuguese, English, Spanish, French, Italian, Ukraine]
        languages.removeFirst();
        System.out.println(languages); //[English, Spanish, French, Italian, Ukraine]
        languages.removeLast();
        System.out.println(languages); //[English, Spanish, French, Italian]
    }

}
