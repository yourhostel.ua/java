package java_21;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static java.lang.StringTemplate.RAW;
import static java.lang.StringTemplate.STR;

public class StringTemplates {

    public static void main(String[] args) {
        String str = "Sergio";
        String message = STR."Hi! \{str}.";
        System.out.println("Використання STR: " + message);

        int x = 10, y = 20;
        String s = STR."\{x} + \{y} = \{x + y}";
        System.out.println("Використання STR 2: " + s);

        StringTemplate raw = RAW."Hi! \{str}.";
        System.out.println("Перетворення RAW: " + raw);

        String messageRaw = STR.process(raw);
        System.out.println("Зворотне перетворення STR.process: " + messageRaw);

        boolean result = true;  // або false
        String msg = STR."\{str} \{result ? "is" : "is not"} present in class.";
        System.out.println(msg);

        String time = STR."The current time is \{
        DateTimeFormatter
                .ofPattern("HH:mm:ss")
                .format(LocalTime.now())
        }.";
        System.out.println("Поточний час в HH:mm:ss: " + time);

        new ThisMethod();
    }

}
