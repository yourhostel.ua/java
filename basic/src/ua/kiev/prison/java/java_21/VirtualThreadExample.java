package java_21;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

public class VirtualThreadExample {
    public static void main(String[] args) {
        ThreadFactory factory = Thread.ofVirtual().factory();
        Runnable task = () -> System.out.println("Hello from virtual thread: " + Thread.currentThread().getName());

        Thread virtualThread = factory.newThread(task);
        virtualThread.setName("VirtualThread-1"); // Встановлення імені потоку
        virtualThread.start();

        try {
            virtualThread.join(); // Очікування завершення віртуального потоку
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        VirtualThreadExecutorExample();
    }

    public static void VirtualThreadExecutorExample() {
        // Створюємо ExecutorService із віртуальними потоками
        ExecutorService executor = Executors.newVirtualThreadPerTaskExecutor();
        List<Future<?>> futures = new ArrayList<>();

        // Надсилаємо кілька завдань на виконання
        for (int i = 0; i < 3; i++) {
            int taskNumber = i;
            Future<?> future = executor.submit(() -> {
                System.out.println("Завдання " + taskNumber + " виконується у потоці: " + Thread.currentThread());
                try {
                    Thread.sleep(1000); // Імітація тривалої операції
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                }
            });
            futures.add(future);
        }

        // Чекаємо на завершення всіх завдань
        futures.forEach(future -> {
            try {
                future.get(); // Очікування завершення завдання
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        // Закриваємо ExecutorService
        executor.shutdown();
    }

}
