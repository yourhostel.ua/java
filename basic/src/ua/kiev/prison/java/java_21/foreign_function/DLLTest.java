package java_21.foreign_function;

import java.lang.foreign.Arena;
import java.lang.foreign.FunctionDescriptor;
import java.lang.foreign.MemorySegment;
import java.lang.foreign.SymbolLookup;
import java.lang.invoke.MethodHandle;
import java.lang.foreign.Linker;
import java.nio.file.Path;

import static java.lang.foreign.ValueLayout.JAVA_INT;

/**
 * <a href="https://docs.oracle.com/en/java/javase/20/core/foreign-function-and-memory-api.html">Foreign Function and Memory API Documentation</a>
 * API для роботи з зовнішніми функціями та пам'яттю (FFM) дозволяє програмам на Java
 * взаємодіяти з кодом та даними, які знаходяться за межами середовища виконання Java.
 * Цей API надає можливість програмам на Java викликати бібліотеки, написані на інших
 * мовах програмування, та обробляти дані, що не належать до Java-середовища, уникаючи
 * при цьому складнощів та ризиків, пов'язаних з використанням JNI. API дозволяє
 * викликати функції, написані на інших мовах, та безпечно працювати з пам'яттю, яка
 * не контролюється JVM.
 * <p>
 * У цьому прикладі використовуються дві функції, написані на C++ і скомпільовані в файл
 * libmath.dll:
 *
 * <pre>{@code
 * #include "pch.h"
 *
 * extern "C" __declspec(dllexport) int add(int a, int b) {
 *     return a + b;
 * }
 *
 * extern "C" __declspec(dllexport) int multiply(int a, int b) {
 *     return a * b;
 * }
 * }</pre>
 */

public class DLLTest {
    public static void main(String[] args) throws Throwable {
        // Створення області для роботи з пам'яттю поза Java heap
        try (Arena arena = Arena.ofConfined()) {

            // Вказуємо шлях до нашої libmath.dll
            Path pathToLib = Path.of("basic/src/ua/kiev/prison/java/java_21/foreign_function/libmath.dll");

            // Створюємо пошуковик для нашої DLL
            SymbolLookup libmath = SymbolLookup.libraryLookup(pathToLib, arena);

            // Знаходимо адреси функцій 'add' та 'multiply' у DLL
            MemorySegment add_addr = libmath.find("add")
                    .orElseThrow(() -> new IllegalStateException("Function 'add' not found"));
            MemorySegment multiply_addr = libmath.find("multiply")
                    .orElseThrow(() -> new IllegalStateException("Function 'multiply' not found"));

            // Створюємо описи сигнатур для цих функцій
            FunctionDescriptor add_sig = FunctionDescriptor.of(JAVA_INT, JAVA_INT, JAVA_INT);
            FunctionDescriptor multiply_sig = FunctionDescriptor.of(JAVA_INT, JAVA_INT, JAVA_INT);

            // Створюємо обробники методів для виклику функцій
            MethodHandle add = Linker.nativeLinker().downcallHandle(add_addr, add_sig);
            MethodHandle multiply = Linker.nativeLinker().downcallHandle(multiply_addr, multiply_sig);

            // Викликаємо функції та виводимо результати
            int sum = (int) add.invokeExact(5, 3);
            int product = (int) multiply.invokeExact(5, 3);

            System.out.println("Sum: " + sum);
            System.out.println("Product: " + product);
        }
    }

}

