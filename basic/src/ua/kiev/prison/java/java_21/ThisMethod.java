package java_21;

class ThisMethod{
    String massage = "Hello";

    ThisMethod(){
        thisMethod();
    }
    String thisTemplate(){
        String thisT = STR."Greetings \{this.massage}!";
        System.out.println("це з використанням this: " + thisT);
        return thisT;
    }

    void thisMethod(){
        System.out.println("Це з використанням методу: " + thisTemplate());
    }

}
