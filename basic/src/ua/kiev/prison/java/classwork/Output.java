package classwork;

import java.io.InputStream;
import java.util.Scanner;

public class Output {
    public static void main(String[] args) {
        InputStream in = System.in;
        Scanner scanner = new Scanner(in);
        System.out.println("Enter your name:");
        String line1 = scanner.nextLine();
        System.out.println("'");
        System.out.println(line1);
        System.out.println("'");
        System.out.println();

        String massage = "Hello, " + line1 + "!";
        System.out.println(massage);
    }
}
