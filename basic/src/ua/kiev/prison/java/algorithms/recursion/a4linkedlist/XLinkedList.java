package algorithms.recursion.a4linkedlist;

import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class XLinkedList {

    class Node {
        int value;
        Node next;

        public Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }

        public Node(int value) {
            this(value, null);
        }

        @Override
        public String toString() {
            return String.format("[%d]", value);
        }
    }

    private Node head;

    public boolean contains(int x) {
        Node curr = head;
        while (curr != null) {
            if (curr.value == x) return true;
            curr = curr.next;
        }
        return false;
    }

    // recursive == tail recursive
    private boolean containsR(int x, Node curr) {
        if (curr == null) return false;
        if (curr.value == x) return true;
        return containsR(x, curr.next);
    }

    public boolean containsR(int x) {
        return containsR(x, head);
    }

    public String toStringIt() {
        StringBuilder sb = new StringBuilder("{");
        Node curr = head;
        while (curr != null) {
            if (curr != head) sb.append(" -> ");
            sb.append(curr);
            curr = curr.next;
        }
        return sb.append("}").toString();
    }

    // TODO: homework
    public String toStringR() {
        StringBuilder sb = new StringBuilder("{");
        toStringR(head, sb);
        return sb.append("}").toString();
    }
    public String toStringR(Node h, StringBuilder sb) {
        if (h == null) return "";
        if (h != head) sb.append(" -> ");
        sb.append(h);
        return toStringR(h.next, sb);
    }
   @Override
    public String toString() {
        return toStringR();
    }

    // add to head
    public void prepend(int x) {
//        Node node = new Node(x);
//        node.next = head;
//        head = node;
        head = new Node(x, head);
    }

    public void append(int x) {
        if (head == null) {
            prepend(x);
            return;
        }

        Node curr = head;
        while (curr.next != null) {
            curr = curr.next;
        }
        curr.next = new Node(x);
    }

    private void appendR1(int x, Node curr) {
        if (curr.next == null) {
            curr.next = new Node(x);
            return;
        }
        appendR1(x, curr.next);
    }

    public void appendR1(int x) {
        if (head == null) {
            prepend(x);
            return;
        }
        appendR1(x, head);
    }

    private Node appendR2(int x, Node curr) {
        if (curr == null) return new Node(x);
        curr.next = appendR2(x, curr.next);
        return curr;
    }

    public void appendR2(int x) {
        head = appendR2(x, head);
    }

    public int length() {
        int l = 0;
        Node curr = head;
        while (curr != null) {
            l++;
            curr = curr.next;
        }
        return l;
    }

    // length recursive - private, implementation
    private int lengthR(Node curr) {
        if (curr == null) return 0;
        return 1 + lengthR(curr.next);
    }

    // length recursive - public, runner part
    public int lengthR() {
        return lengthR(head);
    }

    // length tail recursive - private, implementation
    private int lengthTR(Node curr, int acc) {
        if (curr == null) return acc;
        return lengthTR(curr.next, acc + 1);
    }

    // length tail recursive - public, runner part
    public int lengthTR() {
        return lengthTR(head, 0);
    }

    // TODO: homework
    //  `new Node` is forbidden !!!
    // TODO: homework
    public void reverse() {
        reverse(head,head);
    }
    public void reverse(Node h, Node n) {
        if (n == null) return;
        if (h.next == null) {
            head = n;
            reverse(h, head.next);
            if (n.next != null) append(n.value);
            return;
        }
        reverse(h.next, n);
    }

    public void reverse0() {
        head = reverse0(head, x -> x == null || x.next == null, x -> x.next = null);
    }

    public Node reverse0(Node h, Predicate<Node> p, UnaryOperator<Node> u) {
        if (p.test(h)) return h;
        Node r = reverse0(h.next, p, u);
        h.next.next = h;
        u.apply(h);
        return r;
    }

    public void reverse1() {
        if (head == null || head.next == null) return;
        head.next = reverse1(head);
    }

    public Node reverse1(Node h) {
        if (h.next.next != null) reverse1(h.next);
        else head = h.next;
        h.next.next = h;
        return null;
    }

    public void reverse2() {
        head = reverse2(head);
    }

    public Node reverse2(Node head) {
        if (head == null) return head;
        Node ptr, newHead = head;
        head = head.next;
        newHead.next = null;
        while (head != null) {
            ptr = head.next;
            head.next = newHead;
            newHead = head;
            head = ptr;
        }
        return newHead;
    }

    public void reverse3() {
        head = reverse3(head, null, head);
    }
    public Node reverse3(Node curr, Node prev, Node head) {
        if (curr == null){
            head = prev;
            return head;
        }
        head = reverse3(curr.next, curr, head);
        curr.next = prev;
        return head;
    }

    public void reverse4() {
        head = reverse4(head);
    }
    public Node reverse4(Node h) {
        if (h == null || h.next == null) return h;
        Node r = reverse4(h.next);
        h.next.next = h;
        h.next = null;
        return r;
    }

    public void reverse5() {
        head = reverse5(head);
    }

    public Node reverse5(Node curr) {
        if( curr.next != null) {
            prepend(curr.next.value);
            reverse5(curr.next);
            head = curr.next;
            return head;
        }
        return curr;
    }

    // TODO: homework - remove
    public boolean remove(Predicate<Integer> f) {
        if (head != null && f.test(head.value)) {
            head = head.next;
            return true;
        }
        Optional<Node> found = findNextNode(f);
        if (found.isEmpty()) return false;
        found.ifPresent(x->x.next = x.next.next);
        return true;
    }

    // TODO: homework - remove
    public boolean removeAfter(Predicate<Integer> f) {
        if ((head != null && head.next != null) && f.test(head.value)) {
            head.next = head.next.next;
            return true;
        }
        Optional<Node> found = findNextNode(f);
        if (found.isEmpty() || found.get().next == null) return false;
        if (found.get().next.next == null) return false;
        found.ifPresent(node -> node.next.next = node.next.next.next);
        return true;
    }

    public boolean removeAfter(int i){
        return removeAfter(x -> x == i);
    }

    // TODO: homework - remove
    public boolean removeBefore(Predicate<Integer> f) {
        if (head != null && f.test(head.value)) {
            return false;
        }
        if ((head != null && head.next != null) && f.test(head.next.value)) {
            head = head.next;
            return true;
        }
        Optional<Node> found = findNodePrev(f);
        if (found.isEmpty()) return false;
        found.ifPresent(node -> node.next = node.next.next);
        return true;
    }

    private Optional<Node> findNodePrev(Predicate<Integer> func) {
        Node curr = head;
        if(curr != null && curr.next != null) {
            while (curr.next.next != null) {
                if (func.test(curr.next.next.value)) return Optional.of(curr);
                curr = curr.next;
            }
        }
        return Optional.empty();
    }

    public boolean removeBefore(int i){
        return removeBefore(x -> x == i);
    }
//  public void insertAfter(int x, Predicate<Integer> func)
//  public boolean insertAfter(int x, Predicate<Integer> func)
//  public void    insertAfter(int x, Predicate<Integer> func) throws Exception
//  public void    insertAfterOrTail(int x, Predicate<Integer> func)

    private Optional<Node> findNode(Predicate<Integer> func) {
        Node curr = head;
        while (curr != null) {
            if (func.test(curr.value)) return Optional.of(curr);
            curr = curr.next;
        }
        return Optional.empty();
    }

    private Optional<Node> findNextNode(Predicate<Integer> func) {
        Node curr = head;
        while (curr != null && curr.next != null) {
            if (func.test(curr.next.value)) return Optional.of(curr);
            curr = curr.next;
        }
        return Optional.empty();
    }

    // insert                      f: Int => Boolean
    public boolean insertAfter(int x, Predicate<Integer> func) {
        Optional<Node> found = findNode(func);
        if (found.isEmpty()) return false;
        found.ifPresent(node -> node.next = new Node(x, node.next));
        return true;
    }

    public boolean insertAfter(int x, int n) {
        return insertAfter(x, a -> a == n);
    }

    public boolean insertBefore(int x, Predicate<Integer> func) {
        if (head != null && func.test(head.value)) {
            prepend(x);
            return true;
        }
        Optional<Node> found = findNextNode(func);
        if (found.isEmpty()) return false;
        found.ifPresent(node -> node.next = new Node(x, node.next));
        return true;
    }

    public boolean insertBefore(int x, int n) {
        return insertBefore(x, a -> a == n);
    }

}
