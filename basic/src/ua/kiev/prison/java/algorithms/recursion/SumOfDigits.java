package algorithms.recursion;

public class SumOfDigits {
    static int c = 0;

    public static int sumOfDigits(int n){
        recursion(n);
        return c;
    }
    public static int recursion(int n){
        c += n;
        if (n == 1) return 1;
        return recursion(n - 1);
    }

    public static int sumOfDigits0(int n) {
        if (n == 1) return 1;
        return sumOfDigits0(n - 1) + n;
    }

    //считаем только сумму разрядов
    public static int sumOfDigits1(int n) {
        if (n < 10)return n;
        else return n % 10 + sumOfDigits1(n / 10);
    }

    public static void main(String[] args) {
        System.out.println(sumOfDigits(4));
        System.out.println(sumOfDigits0(12));

        //считаем только сумму разрядов
        System.out.println(sumOfDigits1(2131));
    }
}
