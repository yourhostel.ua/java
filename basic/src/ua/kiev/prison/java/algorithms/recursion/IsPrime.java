package algorithms.recursion;

import java.math.BigInteger;

public class IsPrime {
    public static boolean recursion(int n, int i) {
        // i- дополнительный параметр. При вызове должен быть равен 2
        if (n < 2) {
            return false;
        }
        else if (n == 2) {
            return true;
        }
        else if (n % i == 0) {
            return false;
        }
        else if (i < n / 2) {
            return recursion(n, i + 1);
        } else {
            return true;
        }
    }

    public static boolean primeNumber(long prime) {//перебор делителей
        long j = 0;
        for (long i = 2; i * i <= prime;) {
            if (prime % i == 0) {
                j = 1;
                break;
            }
            i++;
        }
        return j == 0;
        }

   static public boolean MillerRabinPrimalityTest(Integer integer){//встроенная функция
       BigInteger bigInteger = BigInteger.valueOf(integer);
       return bigInteger.isProbablePrime((int) Math.log(integer));
   }

    public static void main(String[] args) {
        //2147483647 - рекорд Эйлера
        System.out.println(recursion(71, 2));
        System.out.println(primeNumber(71));
        System.out.println(MillerRabinPrimalityTest(71));

        System.out.println(primeNumber(2147483647));
        System.out.println(MillerRabinPrimalityTest(2147483647));
    }
}
