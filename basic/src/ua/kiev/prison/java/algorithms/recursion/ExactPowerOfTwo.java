package algorithms.recursion;
/**Точная степень двойки*/
public class ExactPowerOfTwo {
    public static int recursion(double n){
        System.out.println(n);
        if(n == 1) return 1;
        else if(n > 1 && n < 2) return 0;
        else return recursion(n / 2);
    }

    public static String exactPowerOfTwo(int n){
        if(recursion(n) == 1) return "Yes";
        return "No";
    }

//побитовый сдвиг
//У степени двойки установлен всего один бит, у n-1 этот бит будет сброшен и их конъюнкция
// гарантированно даст 0. Условие выполняется только для степени двойки, т.к. при наличии
// более одного установленного бита будет сброшен только младший.

    public static String exactPowerOfTwo0(int n){
        if ((n & (n - 1)) == 0) return "YES";
        else return "NO";
    }


    public static void main(String[] args) {
        System.out.println(exactPowerOfTwo(3));
        System.out.println(exactPowerOfTwo0(3));
    }
}
