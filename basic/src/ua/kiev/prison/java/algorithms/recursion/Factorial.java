package algorithms.recursion;
import java.math.BigInteger;

public class Factorial {
    public static int fact0(int n) {
        if (n == 1)return 1;
        return fact0(n - 1) * n;
    }

    public static BigInteger fact(int n) {
        if (n == 1) return BigInteger.valueOf(1);
        return fact(n - 1).multiply(BigInteger.valueOf(n));
    }
    public static void main(String[] args) {

        System.out.println(fact0(5));
        System.out.println(fact(5));
    }
}
