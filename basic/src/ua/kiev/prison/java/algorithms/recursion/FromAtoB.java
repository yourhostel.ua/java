package algorithms.recursion;

public class FromAtoB {
    public static void increase(int a, int b){
            if (a > b) return;
            System.out.println(a);
        increase(a + 1, b);
    }

    public static void decrease(int a, int b){
        if (a < b) return;
        System.out.println(a);
        decrease(a - 1, b);
    }

    static void fromAtoB(int a, int b){
        if (a < b) increase(a, b);
        else decrease(a, b);
    }

    public static void main(String[] args) {
        fromAtoB(5, 10);
        fromAtoB(10, 5);
    }
}
