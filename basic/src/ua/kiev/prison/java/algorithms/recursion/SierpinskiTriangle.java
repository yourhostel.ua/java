package algorithms.recursion;

public class SierpinskiTriangle {
    public static void sierpinskiTriangle(int depth) {
        entry(depth, 0, init(depth));
    }

    private static int[][] init(int i, int depth, int[][] pT) {
        if (i == depth) return pT;
        pT[i] = new int[depth];
        pT[i][0] = 1;
        pT[i][i] = 1;
        return init(i + 1, depth, pT);
    }

    public static int[][] init(int depth) {
        return init(0, depth, new int[depth][]);
    }

    public static void painter(int i, int j, int[][] pT) {
        if (j == pT.length) return;
        if (j == 1) System.out.print("#");
        if (i != 0 && j != 0) System.out.print((pT[i][j] = (pT[i - 1][j - 1] + pT[i - 1][j]) % 2) == 1 ? "#" : " ");
        painter(i, j + 1, pT);
    }

    public static void entry(int z, int i, int[][] pT) {
        if (i == pT.length) return;
        System.out.print(" ".repeat(z / 2));
        painter(i, 0, pT);
        System.out.println();
        entry(z - 1, i + 1, pT);
    }

    public static void main(String[] args) {
        sierpinskiTriangle((int) Math.pow(2, 4));
    }
}