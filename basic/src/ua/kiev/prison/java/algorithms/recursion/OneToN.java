package algorithms.recursion;

public class OneToN {
    public static void oneToN0(int n){
        if(n < 0) return;
        System.out.println(n);
        oneToN0(n - 1);
    }

    public static String oneToN1(int n){
        if (n == 1) return "1";
        return oneToN1(n - 1)+ " " + n;
    }
//*****************************************************
    public static void oneToN2(int n){
        if(n < 0) return;
        System.out.println(0);
        oneToN2(n - 1, n);
    }

    public static void oneToN2(int n, int t){
        if(n < 0) return;
        System.out.println(t-n);
        oneToN2(n - 1, t);
    }
//******************************************************
    public static void main(String[] args) {
        System.out.println(oneToN1(5));
        oneToN0(5);
        oneToN2(5);
    }
}
