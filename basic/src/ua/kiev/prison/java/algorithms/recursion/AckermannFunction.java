package algorithms.recursion;

public class AckermannFunction {
    public static long ackermannFunction(long m, long n) {
        if (m == 0) {
            return n + 1;
        } else if (n == 0 && m > 0) {
            return ackermannFunction(m - 1, 1);
        } else {
            return ackermannFunction(m - 1, ackermannFunction(m, n - 1));
        }
    }

    public static long ackermannFunction1(long m, long n) {
        if (m == 0) return n + 1;
        if (n == 0) return ackermannFunction1(m - 1, 1);
        return ackermannFunction1(m - 1, ackermannFunction1(m, n - 1));
    }

    public static long ackermannFunction0(long m, long n) {
        while (n != 0) {
            if (m == 0) m = 1;
            else m = ackermannFunction0(n, m - 1);
            n--;
        }
        return m + 1;
    }

    public static void main(String[] args) {
        System.out.println(ackermannFunction1(3, 12));
        System.out.println(ackermannFunction(2, 1));
        System.out.println(ackermannFunction0(2, 1));
    }
}
