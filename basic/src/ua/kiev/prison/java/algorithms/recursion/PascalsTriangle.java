package algorithms.recursion;
import java.math.BigInteger;

public class PascalsTriangle {

    private static final String icon = " ^";

    public static void pascalsTriangle(int k, int n){
            if (n > k) return;
            System.out.print(" ".repeat(k - n));
            inner(1, n);
            System.out.println();
            pascalsTriangle(k, n + 1);
    }

    public static void inner(int m, int n) {
        if (n < m) return;
        if (m == 1 || m == n) System.out.print(icon);
        else paint(1, m, n);
        inner(m + 1, n);
    }

    public static void paint(int s, int m, int n){
        if (m == s + 1 || m == n - s || s <= n) {
            System.out.printf("%s",
                    binomialCoefficient(n - 1, m - 1)
                            .mod(BigInteger.valueOf(2))
                            .compareTo(BigInteger.valueOf(0)) == 0 ? "  " : icon);
            return;
        }
        paint(s + 1, m, n);
    }

    public static BigInteger binomialCoefficient(int n, int m){
        return factorial(n)
                .divide((factorial(m)
                        .multiply(factorial(n - m))));
    }

    public static BigInteger factorial(int n){
        if (n == 1) return BigInteger.valueOf(1);
        return factorial(n - 1).multiply(BigInteger.valueOf(n));
    }

    public static void main(String[] args) {
        pascalsTriangle(((int) Math.pow(2, 9)), 1);
    }
}
