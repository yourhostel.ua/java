package algorithms.binaryTree;

record Pair<K, V>(K key, V value) {}
