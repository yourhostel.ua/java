package algorithms.binaryTree;

import java.util.LinkedList;
import java.util.Queue;
import algorithms.binaryTree.BinaryTree.Node;

public class BinarySearch {
    public static void main(String[] args) {
        BinaryTree<Integer, Character> bt = new BinaryTree<>();
        for (int i = 0, j = 65; i < 10; i++, j++){
            bt.put(i, (char) j);
        }

        System.out.println(bt);
        System.out.println("DFS. Алгоритмы в глубь имеют три типа обходов:");
        System.out.print(" pre-order: ");
        preOrder(bt.root);
        System.out.println();

        System.out.print("  in-order: ");
        inOrder(bt.root);
        System.out.println();

        System.out.print("post-order: ");
        postOrder(bt.root);
        System.out.println();

        System.out.print("       BFS: ");
        bfs(bt.root).forEach(x-> System.out.print(x + " "));
        System.out.println();
    }

    /*
     * Depth-first в глубину
     */
    public static void preOrder(Node node){
        if (node == null) return;
        System.out.print(node.value + " ");
        preOrder(node.left);
        preOrder(node.right);
    }

    public static void inOrder(Node node){
        if (node == null) return;
        inOrder(node.left);
        System.out.print(node.value + " ");
        inOrder(node.right);
    }

    public static void postOrder(Node node){
        if (node == null) return;
        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.value + " ");
    }
    /*
     * Breadth-first  в ширину
     */
    public static Queue<Character> bfs(Node node){
        Queue<Node> queue = new LinkedList<>();
        Queue<Character> values = new LinkedList<>();
        queue.add(node);

        while(queue.size() > 0){
            Node tempNode = queue.poll();
            values.add((Character) tempNode.value);
            if (tempNode.left != null) queue.add(tempNode.left);
            if (tempNode.right != null) queue.add(tempNode.right);
        }

        return values;
    }
}
