package algorithms.binaryTree;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BinaryTree <K extends Comparable<K>, V> {

        class Node {
            K key;
            V value;
            Node left;
            Node right;
            int n = 1;

            public Node(K key, V value) {
                this.key = key;
                this.value = value;
            }

            public int diff() {
                return nodeSize(this.left) - nodeSize(this.right);
            }

            public void updateSize() {
                this.n = nodeSize(this.left) + 1 + nodeSize(this.right);
            }
        }

        public Node root;

        Optional<Pair<K, V>> contains(K key) {
            Node node = root;
            while (node != null) {
                int cmp = key.compareTo(node.key);
                if (cmp < 0) node = node.left;
                else if (cmp > 0) node = node.right;
                else return Optional.of(new Pair<>(key, node.value));
            }
            return Optional.empty();
        }

        private Optional<Pair<K, V>> containsR(K key, Node node) {
            if (node == null) return Optional.empty();
            int cmp = key.compareTo(node.key);
            if (cmp < 0) return containsR(key, node.left);
            else if (cmp > 0) return containsR(key, node.right);
            else return Optional.of(new Pair<>(key, node.value));
        }

        Optional<Pair<K, V>> containsR(K key) {
            return containsR(key, root);
        }

        int nodeSize(Node x) {
            return x == null ? 0 : x.n;
        }

        Node put(Node x, K key, V value) {
            if (x == null) return new Node(key, value);
            int cmp = key.compareTo(x.key);
            if (cmp < 0) x.left = put(x.left, key, value);
            else if (cmp > 0) x.right = put(x.right, key, value);
            else x.value = value;
            x.updateSize();
            return rebalance(x);
        }

        void put(K key, V value) {
            root = put(root, key, value);
        }

        void put(K key) {
            put(key, null);
        }

        private Node findMinWithLeftFree(Node x) {
            return x.left == null ? x : findMinWithLeftFree(x.left);
        }

        private Node deleteMinAndPullUp(Node x) {
            if (x.left == null) return x.right;
            x.left = deleteMinAndPullUp(x.left);
            return x;
        }

        private Node performRemoval(Node x) {
            Node leftSubTree = x.left;
            Node attachTo = findMinWithLeftFree(x.right);
            Node newRight = deleteMinAndPullUp(x.right);
            attachTo.right = newRight;
            attachTo.left = leftSubTree;
            return attachTo;
        }

        Node remove(Node x, K key) {
            if (x == null) return null;
            int cmp = key.compareTo(x.key);
            if (cmp < 0) x.left = remove(x.left, key);
            else if (cmp > 0) x.right = remove(x.right, key);
            else /* cmp == 0 */ {
                if (x.left == null) return x.right;
                if (x.right == null) return x.left;
                x = performRemoval(x);
            }
            return x;
        }

        void remove(K key) {
            root = remove(root, key);
        }

        FindResult<K, V> find(K key) {
            Node node = root;
            Node bigger = null;
            Node smaller = null;
            while (node != null) {
                int cmp = key.compareTo(node.key);
                if (cmp < 0) {
                    bigger = node;
                    node = node.left;
                } else if (cmp > 0) {
                    smaller = node;
                    node = node.right;
                } else return new FindResult.Found<>();
            }
            return new FindResult.NotFound<>(
                    smaller == null ? null : new Pair<>(smaller.key, smaller.value),
                    bigger == null ? null : new Pair<>(bigger.key, bigger.value)
            );
        }

        int count(Node node) {
            return node == null ? 0 : 1 + count(node.left) + count(node.right);
        }

        int count() {
            return count(root);
        }

        Optional<K> min(Node node) {
            if (node == null) return Optional.empty();
            if (node.left == null) return Optional.of(node.key);
            return min(node.left);
        }

        Optional<K> min() {
            return min(root);
        }

        public Node rotateRight(Node node) {
            Node n = node.left;
            node.left = n.right;
            n.right = node;

            node.updateSize();
            n.updateSize();

            return n;
        }

        private Node rotateLeft(Node node) {
            Node n = node.right;
            node.right = n.left;
            n.left = node;

            node.updateSize();
            n.updateSize();

            return n;
        }

        private Node rebalance(Node node) {
            int diff = node.diff();
            if (Math.abs(diff) < 2) return node;
            return diff > 0 ? rotateRight(node) : rotateLeft(node);
        }

        K treeMax(Node r) {
            while (r.right != null) r = r.right;
            return r.key;
        }

        K treeMax() {
            return treeMax(root);
        }

        K treeMin(Node r) {
            while (r.left != null) r = r.left;
            return r.key;
        }

        K treeMin() {
            return treeMin(root);
        }

        int maxDepth(Node r) {
            if (r == null) return 0;
            int lDepth = maxDepth(r.left);
            int rDepth = maxDepth(r.right);
            if (lDepth > rDepth) return (lDepth + 1);
            return (rDepth + 1);
        }

        int maxDepth() {
            return maxDepth(root);
        }

        private int depth(Node node) {
            return node == null ? 0 : 1 + Math.max(depth(node.left), depth(node.right));
        }

        public int depth() {
            return depth(root);
        }

        public int bWidth() {
            return 1 << depth();
        }// 2^depth

        @Override
        public String toString() {
            List<Node> c = new ArrayList<>();
            c.add(root);
            return s(c, 1, 1<<depth(), (1<<depth()) - 2, new StringBuilder(), depth());
        }
        public String s(List<Node> c, int count, int space_variable, int edge_variable,  StringBuilder sb, int depth) {
            if (count >= depth + 1) return String.valueOf(sb);
            int space = space_variable / 2;
            int edge = edge_variable - space_variable / 2;
            List<Node> newC = new ArrayList<>();
            if (!c.isEmpty()) {
                for (Node node : c) {
                    if (node == null) {
                        sb.append(buildLine(edge_variable, space_variable, ""));
                    } else {
                        sb.append(buildLine(edge_variable, space_variable, String.valueOf(node.value)));
                        if (node.left != null) newC.add(node.left); else newC.add(null);
                        if (node.right != null) newC.add(node.right); else newC.add(null);
                    }
                }
                sb.append("\n");
            }
            return s(newC, count + 1, space, edge, sb, depth);
        }
        String buildLine(int edge_variable, int space_variable, String n) {
            return n.length() != 0 ? String.format("%s%s%4s%s%s",
                    " ".repeat(space_variable),
                    "_".repeat(edge_variable),
                    n,
                    "_".repeat(edge_variable),
                    " ".repeat(space_variable)
            ) : String.format("%s    ",
                    " ".repeat((space_variable + edge_variable) * 2)
            );
        }
    }

