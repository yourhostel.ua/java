package algorithms.LinkedHashMap0;

import java.util.LinkedHashMap;
import java.util.Map;

public class SimpleCache<K, V> extends LinkedHashMap<K, V> {
    private final int capacity;
    public SimpleCache(int capacity){
        super(capacity + 1, 1.1f, true);//capacity + 1 - для отрабатывания removeEldestEntry; 1.1 - не расширятся,true - елемент обращения помещается в конец
        this.capacity = capacity;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K,V> eldest){//очистить хеш при условии
        return this.size() > capacity;
    }
}

//https://habr.com/ru/articles/129037/
//https://www.youtube.com/watch?v=-S_huEuNJiU&list=PL6jg6AGdCNaWtTjsYJ9t0VaITpIZm4pMt
