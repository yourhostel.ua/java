package algorithms.LinkedHashMap0;

public class Run {
    public static void main(String[] args) {
        SimpleCache<Integer, String> sk = new SimpleCache<>(2);
        sk.put(1, "a");
        sk.put(2, "b");
        sk.put(3, "c");
        System.out.println(sk.get(2));
        sk.put(9, "d");
        System.out.println(sk);
    }
}
