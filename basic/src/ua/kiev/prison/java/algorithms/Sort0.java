package algorithms;

import java.util.Arrays;
import java.util.Random;

public class Sort0 {
    static int[] merge(int[] xs, int[] ys) {
        int[] zs = new int[xs.length + ys.length];
        int i = 0; // xs
        int j = 0; // ys
        int k = 0; // zs
        while (i < xs.length && j < ys.length)
            zs[k++] = xs[i] < ys[j] ? xs[i++] : ys[j++];
        while (i < xs.length)
            zs[k++] = xs[i++];
        while (j < ys.length)
            zs[k++] = ys[j++];
        return zs;
    }

    static int[] sort(int[] items){
        if (items.length < 2) return items;
        int middle = items.length / 2;
        int[] xs = Arrays.copyOf(items, middle);
        int[] ys = Arrays.copyOfRange(items, middle, items.length);
        return merge(sort(xs), sort(ys));
    }

    public static void main(String[] args) {
        int[] zs = new Random().ints(5, 10, 90).toArray();
        System.out.println(Arrays.toString(sort(zs)));
    }
}
