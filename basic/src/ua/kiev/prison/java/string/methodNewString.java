package string;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class methodNewString {
    public static void main(String... args) {
        String habr = "habrahabr";
        char[] habrAsArrayOfChars = {'h', 'a', 'b', 'r', 'a', 'h', 'a', 'b', 'r'};
        byte[] habrAsArrayOfBytes = {104, 97, 98, 114, 97, 104, 97, 98, 114};
        System.out.println(new String(habr.toCharArray(), 0, 2));
        System.out.println(new String(habrAsArrayOfBytes, 0, 4));
        System.out.println(new String(habrAsArrayOfChars));
        System.out.println(new String(habrAsArrayOfBytes, StandardCharsets.UTF_16BE));
        System.out.println(new String(new StringBuffer(Arrays.toString(habrAsArrayOfBytes))));
        System.out.println(new String(new StringBuilder(habr)));
        char h = 'a';
        boolean isFound = false;
        for (int i = 0; i < habr.length(); ++i) {
            if (habr.charAt(i) == h) {
                isFound = true;
                break; // первое вхождение
            }
        }
        System.out.println(isFound);
        System.out.println(habr.indexOf('r'));

        String javaHub = "habrhabr".concat(".ru").concat("/hub").concat("/java");
        System.out.println(javaHub);
        System.out.println(message(false));

        String habra = "habra";
        System.out.println(new StringBuilder().append(habra).append(habr).toString());
    }
    private static String message(boolean b) {
        return "Your char had".concat(b ? " " : "n't ").concat("been found!");
    }
}
