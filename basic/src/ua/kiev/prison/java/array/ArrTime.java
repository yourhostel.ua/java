package array;

import libs.Console;

public class ArrTime {
    public static void main(String[] args) {
        int[][] g = new int[5][5];
        for(int i = 0; i < g.length; i++) {
            g[i][i] = 2 * i;// диагональ или i + i или i << 1
            for(int j = 0; j < g[i].length; j++) {
                //g[i][j] = i + j;
                g[j][i] = g[i][j] = i + j;
            }
        }

        for (int[] ints : g) {
            for (int anInt : ints) {
                System.out.printf("%d    ", anInt);
            }
            Console.log("\n");
        }
        test();
    }
    static void test(){
        int n = 8000;

        int[][] g = new int[n][n];
        long st, en;

        // one
        st = System.nanoTime();
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                g[i][j] = i + j;
            }
        }
        en = System.nanoTime();
        System.out.println("\nOne time " + (en - st)/1000000.d + " msc");

        // two
        st = System.nanoTime();
        for(int i = 0; i < n; i++) {
            g[i][i] =  i + i;
            for(int j = 0; j < i; j++) {
                g[j][i] = g[i][j] = i + j;
            }
        }
        en = System.nanoTime();
        System.out.println("\nTwo time " + (en - st)/1000000.d + " msc");
    }

}
