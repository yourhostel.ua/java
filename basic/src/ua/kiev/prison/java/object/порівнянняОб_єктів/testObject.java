package object.порівнянняОб_єктів;

import libs.Console;

public class testObject {

    public static void main(String[] args) {
        Person person = new Person("Tom");
        Console.log(String.format("hash code: %s", person.hashCode()));
    }
    static class Person {
        private final String name;
        public Person(String name){
            this.name = name;
        }
        @Override
        public int hashCode(){
            return 10 * name.hashCode() + 20456;
        }
    }
}


