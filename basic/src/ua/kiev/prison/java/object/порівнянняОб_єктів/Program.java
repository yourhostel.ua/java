package object.порівнянняОб_єктів;

public class Program{

    public static void main(String[] args) {

        User tom = new User("Tom");
        User bob = new User("Bob");
        System.out.printf("tom.equals(bob) => %b\n", tom.equals(bob)); // false

        User tom2 = new User("Tom");
        System.out.printf("tom.equals(tom2) => %b\n", tom.equals(tom2)); // true
    }
    static class User {

        private final String name;

        public User(String name){

            this.name=name;
        }

        @Override
        public boolean equals(Object o){

            if (!(o instanceof User)) return false;

            return this.name.equals(((User)o).name);
        }
    }
}

