package conditionalStatements;
import java.util.*;

public class IfElse {
    private static final Scanner scan = new Scanner(System.in);
    public static void main(String[] args) {

//        System.out.println("введіть: x");
//        int x = scan.nextInt();
//        if (x < 0) {
//            System.out.println("число " + x + " від'ємне");
//        } else if (x == 0){
//            System.out.println("a: 0");
//        } else {
//            System.out.println("число " + x + " додатне" );
//        }

        //hackerrank.com
        /*
         * If a is odd, print Weird
         * If a is even and in the inclusive range of 2 to 5, print Not Weird
         * If a is even and in the inclusive range of 6 to 20, print Weird
         * If a is even and greater than , print Not Weird
         */

        int a = scan.nextInt();

        if (a % 2 != 0 && a < 20) {
            System.out.println("Weird");
        }else if (a % 2 == 0 && a >= 2 && a <= 5) {
            System.out.println("Not Weird");
        }else if (a % 2 == 0 && a >= 6 && a <= 20) {
            System.out.println("Weird");
        }else if (a % 2 == 0 && a > 20) {
            System.out.println("Not Weird");
        }
        if (a % 2 != 0 && a > 20) {
            System.out.println("Weird");
        }
        scan.close();
    }
}

