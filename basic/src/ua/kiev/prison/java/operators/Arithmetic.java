package operators;

/**
 *      +	   Addition	        Adds together two values	               x + y
 *      -	   Subtraction	    Subtracts one value from another	       x - y
 *      *	   Multiplication	Multiplies two values	                   x * y
 *      /	   Division	        Divides one value by another	           x / y
 *      %	   Modulus	        Returns the division remainder	           x % y
 *      ++	   Increment	    Increases the value of a variable by 1	     ++x
 *      --	   Decrement	    Decreases the value of a variable by 1	     --x
 */
//https://www.w3schools.com/java/java_operators.asp
public class Arithmetic {
    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        int c = 25;
        int d = 25;
        int k = 576;
        int m = 575;
        System.out.println("a + b = " + (a + b));
        System.out.println("a - b = " + (a - b));
        System.out.println("a * b = " + (a * b));
        System.out.println("b / a = " + (b / a));
        System.out.println("b % a = " + (b % a));
        System.out.println("c % a = " + (c % a));
        System.out.println("a++   = " +  (a++));
        System.out.println("b--   = " +  (a--));
        // Проверьте разницу в d++ и ++d
        System.out.println("d++   = " +  (d++));
        System.out.println("++d   = " +  (++d));
    }
}

/*
 *    =	    x = 5	  x = 5
 *    +=   	x += 3	  x = x + 3
 *    -=   	x -= 3	  x = x - 3
 *    *=   	x *= 3	  x = x * 3
 *    /=   	x /= 3	  x = x / 3
 *    %=   	x %= 3	  x = x % 3
 *    &=   	x &= 3	  x = x & 3
 *    |=   	x |= 3	  x = x | 3
 *    ^=   	x ^= 3	  x = x ^ 3
 *    >>=	x >>= 3	  x = x >> 3
 *    <<=	x <<= 3	  x = x << 3
 */
