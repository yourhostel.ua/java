package operators;

import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * Оператор	Описание	Пример
 * & (побитовое и)	Бинарный оператор AND копирует бит в результат, если он существует в обоих операндах.	(A & B) даст 12, который является 0000 1100
 * | (побитовое или)	Бинарный оператор OR копирует бит, если он существует в любом из операндов.	(A | B) даст 61 который равен 0011 1101
 * ^ (побитовое логическое или)	Бинарный оператор XOR копирует бит, если он установлен в одном операнде, но не в обоих.	(A ^ B) даст 49, которая является 0011 0001
 * ~ (побитовое дополнение)	Бинарный оператор дополнения и имеет эффект «отражения» бит.	(~ A) даст -61, которая является формой дополнением 1100 0011 в двоичной записи
 * << (сдвиг влево)	Бинарный оператор сдвига влево. Значение левых операндов перемещается влево на количество бит, заданных правым операндом.	A << 2 даст 240, который 1111 0000
 * >> (сдвиг вправо)	Бинарный оператор сдвига вправо. Значение правых операндов перемещается вправо на количество бит, заданных левых операндом.	A >> 2 даст 15, который является 1111
 * >>> (нулевой сдвиг вправо)	Нулевой оператор сдвига вправо. Значение левых операндов перемещается вправо на количество бит, заданных правым операндом, а сдвинутые значения заполняются нулями.	A >>> 2 даст 15, который является 0000 1111
 */
public class Bitwise {

    public static void main(String[] args) {
        int a = 60;	/* 60 = 0011 1100 */
        int b = 13;	/* 13 = 0000 1101 */

        System.out.println(Integer.toBinaryString(5));

                                                                            /* 60 = 0011 1100 */
                                                                            /* 13 = 0000 1101 */
        System.out.printf("a & b = %d (%s)\n", (a & b), intToBin(a & b) );/* 12 = 0000 1100 */


                                                                            /* 60 = 0011 1100 */
                                                                            /* 13 = 0000 1101 */
        System.out.printf("a | b = %d (%s)\n", (a | b), intToBin(a | b) );/* 61 = 0011 1101 */

                                                                            /* 60 = 0011 1100 */
                                                                            /* 13 = 0000 1101 */
        System.out.printf("a ^ b = %d (%s)\n", (a ^ b), intToBin(a ^ b) );/* 49 = 0011 0001 */

                                                                            /* 60 = 0011 1100 */
                                                                            /* 13 = 0000 1101 */
        System.out.printf("~a = %d (%s)\n", (~a), intToBin((~a)) );         /*-61 = 1100 0011 */


                                                                               /*  60 = 0011 1100 */
        System.out.printf("a << 2 = %d (%s)\n", (a << 2), intToBin(a << 2)); /* 240 = 1111 0000 */


                                                                                /* 60 = 0011 1100 */
        System.out.printf("a >> 2  = %d (%s)\n", (a >> 2), intToBin(a >> 2) );/* 15 = 0000 1111 */


                                                                                  /*  60 = 0011 1100 */
        System.out.printf("a >>> 2 = %d (%s)\n", (-8 >>> 2), intToBin(-8 >>> 2) );/* 215 = 0000 1111 */
        //System.out.println(bitToInt("00010000"));
        System.out.println(intToBin(1073741822));
        System.out.println(intToBin(4));
        System.out.println(intToBin(254));
        int x = 12;
        System.out.println(x >> 1);
        System.out.println(intToBin2("00001111"));
        System.out.println(1<<4);//4 в степени 2 = 16
        System.out.println(5<<1);// 5 * 2 = 10
        System.out.println(10>>1);  // 10 / 2 = 5
        System.out.println(9>>1);  // 9 / 2 = 4 // деление с округлением в сторону отрицательной бесконечности
        System.out.println(250>>1); //250 // 2 = 125
        System.out.println(~-20 + 1); // 20
    }
    public static String intToBin(int x) {
        return IntStream.rangeClosed(0, 31)     // 0..7
                .map(i -> 31 - i)               // 7..0
                .map(i -> (x >> i) & 1)        // 0 / 1
                .mapToObj(Integer::toString)   // "0" / "1"
                .collect(Collectors.joining());
    }
    static String intToBin1(int x) {
        StringBuilder bin = new StringBuilder();
        while (x > 0) {
            bin.append(
                    x & 1
//                  x % 2
            );
//          x = x / 2;
            x = x >> 1;
        }
        String data = bin.reverse().toString();
        return "0".repeat(8 - data.length()) + data;
    }
    static int intToBin2(String raw) {
        int outcome = 0;
        for(int i = 0; i < raw.length(); i++) {
            //System.out.printf("raw.charAt(i) = %s raw.charAt(i)-'0' = %s\n", typeOf(raw.charAt(i)), typeOf(raw.charAt(i)-'0'));
            outcome += (raw.charAt(i) - '0') * (1 << (raw.length() - i - 1));
        }
        return outcome;
    }
}
