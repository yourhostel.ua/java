package hackerRank;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class StaticInitializerException {
    static int B;
    static int H;
    public static void main(String[] args) {
        PrintStream out = System.out;
        InputStream in = System.in;
        var scanner = new Scanner(in);

        try{
            B = scanner.nextInt();
            H = scanner.nextInt();
            if(B <= 0 || H <= 0){
                throw new Exception("Breadth and height must be positive");
            } else out.printf("%d", B * H);

        }catch (Exception ex){
            out.printf("%s", ex);
        }
    }
}
