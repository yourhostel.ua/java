package hackerRank;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
/**
 * ([A-Z])(.+)     Valid
 * [AZ[a-z](a-z)   Invalid
 * batcatpat(nat   Invalid
 */
public class PatternCompile {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int testCases = Integer.parseInt(in.nextLine());
        while(testCases>0){
            String pattern = in.nextLine();
            PatternSyntaxException exception = null;
            try{
                Pattern.compile(pattern);

            }
            catch(PatternSyntaxException err)
            {
                System.out.println("Invalid");
                exception = err;

            }
            if(exception == null)
                System.out.println("Valid");
            testCases--;
        }
    }
}
