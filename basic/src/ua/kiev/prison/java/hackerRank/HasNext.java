package hackerRank;

import java.io.PrintStream;
import java.util.Scanner;

public class HasNext {
    public static void main(String[] args) {
        PrintStream out = System.out;
        var scanner = new Scanner("Hello world \nI am a file \nRead me until end-of-file.");

        int n = 1;
        while (scanner.hasNext()) {
            String a = scanner.nextLine();
            out.printf("%d %s\n", n, a);
            n++;
        }
    }
}
