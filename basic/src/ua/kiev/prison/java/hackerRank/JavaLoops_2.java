package hackerRank;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static java.lang.Math.pow;

public class JavaLoops_2 {
    public static void main(String[] args) {
        PrintStream out = System.out;
        InputStream in = System.in;
        var scanner = new Scanner(in);
        int q = scanner.nextInt();
        scanner.nextLine();
        Integer[][] arrResult = new Integer[q][];
        for (byte i = 0; i < q; i++) {

            String k = scanner.nextLine();

            int a = Integer.parseInt(k.split(" ")[0]);
            int b = Integer.parseInt(k.split(" ")[1]);
            int n = Integer.parseInt(k.split(" ")[2]);

            Integer[] arrArr = new Integer[n];

            for (int m = 0; m <= n; m++) {
                for (int l = 0; l <= m; l++) {
                    int[] arrMore = new int[n + 1];

                        for (int f = 0; f <= l; f++) {
                            if(f == 0){
                                arrMore[0] = (int) (a + pow(2, 0) * b);
                            } else arrMore[f] = (int) (pow(2, f) * b);
                        }

                    if (l == (m - 1)) {
                        int sum = 0;
                        for (int h : arrMore) sum += h;
                        arrArr[l] = sum;
                    }
                }
            }

            arrResult[i] = arrArr;

            if (i == q - 1) {
                for (Integer[] integers : arrResult) {
                    for (Integer integer : integers) {
                        out.printf("%d ", integer);
                    }
                    out.print("\n");
                }
            }
        }





//недочитывает строку только копипаст

//        Scanner in = new Scanner(System.in);
//        int t=in.nextInt();
//        for(int i=0;i<t;i++){
//            int a = in.nextInt();
//            int b = in.nextInt();
//            int n = in.nextInt();
//            int  p = a;
//            System.out.println();
//            for(int j = 0;j<n;j++)
//            {
//                int k = 0;
//                k = (int)Math.round((Math.pow(2,j)))*b;
//                p += k;
//                System.out.printf("%d",p);
//                System.out.print(" ");
//            }
//        }
//        in.close();
    }
}
