package hackerRank;

import java.util.Scanner;
/**
 * 8
 * Julia            Invalid
 * Samantha         Valid
 * Samantha_21      Valid
 * 1Samantha        Invalid
 * Samantha?10_2A   Invalid
 * JuliaZ007        Valid
 * Julia@007        Invalid
 * _Julia007        Invalid
 */
class UsernameValidator {
    public static final String regularExpression = "[A-Za-z][\\w]{7,29}";
}
public class UsernameChecker {
    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int n = Integer.parseInt(scan.nextLine());
        while (n-- != 0) {
            String userName = scan.nextLine();

            if (userName.matches(UsernameValidator.regularExpression)) {
                System.out.println("Valid");
            } else {
                System.out.println("Invalid");
            }
        }
    }
}

