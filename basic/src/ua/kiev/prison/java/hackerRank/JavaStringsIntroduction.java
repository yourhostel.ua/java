package hackerRank;

import libs.Console;
import static libs.Console.nextLine;
import static libs.StringMethod.capitalize;

public class JavaStringsIntroduction {

    public static void main(String[] args) {
        String A = nextLine();
        String B = nextLine();
        Console.log(String.format("%s",A.length() + B.length()));
        Console.log(A.compareTo(B) >0 ? "Yes" : "No");
        Console.log(capitalize(A) + " " + capitalize(B));

//        Scanner sc=new Scanner(System.in);
//        String A=sc.next();
//        String B=sc.next();
//        System.out.println(String.format("%s",A.length() + B.length()));
//        System.out.println(A.compareTo(B) >0 ? "Yes" : "No");
//        System.out.println(A.substring(0, 1).toUpperCase() + A.substring(1) + " " + B.substring(0, 1).toUpperCase() + B.substring(1));
    }
}
