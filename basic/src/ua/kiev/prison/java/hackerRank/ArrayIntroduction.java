package hackerRank;
import java.util.Scanner;

public class ArrayIntroduction {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        int l = scanner.nextInt();
        int[] myArray = new int[l];
        for(int i = 0; i < l; i++){
            myArray[i] = scanner.nextInt();
        }
        for(int i = 0; i < l; i++){
            System.out.println(myArray[i]);
        }
    }
}
