package hackerRank;

import java.math.BigInteger;
import java.util.Scanner;

public class Prime {
    public static void main(String[] args) {
           Scanner scanner = new Scanner (System.in);
           BigInteger a = scanner.nextBigInteger();
           BigInteger i = new BigInteger( "2");
            int j = 0;
            for(;a.compareTo(i.multiply(i)) >= 0 && j != 1; i = i.add(i)){
                if(a.mod(i).equals(new BigInteger( "0"))) j = 1;
            }
            if(j == 1 || a.equals(BigInteger.valueOf(1))){
                System.out.println("not prime");
                return;
            }
            System.out.println("prime");

//        BigInteger c = scanner.nextBigInteger();
//        boolean probablePrime = c.isProbablePrime(1);
//        if(probablePrime) System.out.println("prime");
//                     else System.out.println("not prime");
    }
}
