package hackerRank;

import java.util.Scanner;

public class NegativeSubArray {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for(int i = 0; i < n; i++){
            arr[i] = scanner.nextInt();
        }

        int x = 0;

        for(int i = 0; i < n; i++){
            int[] y = new int[i + 1];
            for(int j = 0; j < n - i ; j++) {
                if ((arrSum(subArr(arr, j, y)) < 0)) x++;
            }
        }
        System.out.println(x);
    }

    static int arrSum(int[] arr){
        int x = 0;
        for (int j : arr) {x += j;}
        return x;
    }

    static int[] subArr(int[] arr, int j, int[] subArr){
        System.arraycopy(arr, j, subArr, 0, subArr.length);
        return subArr;
    }
}