package hackerRank;

import libs.Console;

import java.util.Scanner;

public class StringTokens {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        //String str = s.replaceAll("\\pP", " ");
        String[] tokens = s.trim().split("[ !,?\\._'@]+",0);
        if (tokens.length == 1 && tokens[0].equals("")){Console.log(String.format("%d",0));}
        else {
            Console.log(String.valueOf(tokens.length));
            for (String token: tokens){ System.out.printf("%s%n",token);}
        }
        scan.close();
    }
}
