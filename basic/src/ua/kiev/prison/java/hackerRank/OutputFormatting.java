package hackerRank;

import java.io.PrintStream;
import java.util.Locale;
import java.util.Scanner;
import java.io.InputStream;

public class OutputFormatting {
    public static void main(String[] args) {
        InputStream in = System.in;
        var scanner = new Scanner(in).useLocale(Locale.US);


        String stA = scanner.nextLine();
        String stA_A = stA.split(" ")[0];
        int stA_B = Integer.parseInt(stA.split(" ")[1]);
        String stB = scanner.nextLine();
        String stB_A = stB.split(" ")[0];
        int stB_B = Integer.parseInt(stB.split(" ")[1]);
        String stC = scanner.nextLine();
        String stC_A = stC.split(" ")[0];
        int stC_B = Integer.parseInt(stC.split(" ")[1]);

        PrintStream out = System.out;
        out.print("================================\n");
        out.printf("%-11s    %0,3d\n",stA_A,stA_B);
        out.printf("%-11s    %0,3d\n",stB_A,stB_B);
        out.printf("%-11s    %0,3d\n",stC_A,stC_B);
        out.print("================================\n");
    }
}
