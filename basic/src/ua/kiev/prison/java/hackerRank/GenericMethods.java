package hackerRank;

public class GenericMethods {
    public static void main(String[] args) {
        int[] b = new int[]{1, 2, 3};
        String[] c = new String[]{"Hello", "World"};
        printArray(b);
        printArray(c);

    }
    static void printArray(String[] a){
        for(String i: a){System.out.println(i);}

    }
    static void printArray(int[] a){
        for(int i: a){System.out.println(i);}
    }
}
