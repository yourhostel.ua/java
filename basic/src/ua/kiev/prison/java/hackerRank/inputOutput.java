package hackerRank;

import java.io.PrintStream;
import java.util.Locale;
import java.util.Scanner;
import java.io.InputStream;

//import static java.util.regex.Pattern.matches;
public class inputOutput {
    public static void main(String[] args) {

        InputStream in = System.in;
        var scanner = new Scanner(in).useLocale(Locale.US);

        int i = scanner.nextInt();

        Double db = scanner.nextDouble();
        scanner.nextLine();
        String stA = scanner.nextLine();


        PrintStream out = System.out;
        out.printf("String: %s\n",stA);
        out.printf("Double: %s\n",db);
        out.printf("Int: %s\n",i);
    }
}


//        if(matches("^([0-9]*\\.[0-9]+)$", db)) {
//            db = String.format("%s",db);
//        } else {
//            db = String.format("%s.0",db);
//        }

