package hackerRank.problemSolving;

import java.util.ArrayList;
import java.util.List;

public class AliceAndBob {
    public static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        List<Integer> c = new ArrayList<>();

        Integer Alice = 0;
        Integer Bob = 0;
        for(int i = 0; i < a.size(); i++){
            if(a.get(i) > b.get(i)) Alice++;
            if(a.get(i) < b.get(i)) Bob++;
        }
        c.add(Alice);
        c.add(Bob);
        return c;
    }
}
