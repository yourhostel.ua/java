package hackerRank.problemSolving;

import java.util.*;

public class BetweenTwoSets {
    public static int getTotalX(List<Integer> a, List<Integer> b) {
        Set<Integer> set = new TreeSet<>();
        for (int k = 1; k <= 100; k++){
            for(int i: a) {
                for (int j: b){
                    if(!(k % i == 0 && j % k == 0)) {
                        set.add(k);
                    }
                }
            }
        }
        System.out.println(set);

        return 100 - set.size();
    }

    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>(List.of(new Integer[]{2, 4}));
        List<Integer> b = new ArrayList<>(List.of(new Integer[]{16, 32, 96}));

        System.out.println(getTotalX(a, b));
    }
}
