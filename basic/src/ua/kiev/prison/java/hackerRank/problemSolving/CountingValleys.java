package hackerRank.problemSolving;

public class CountingValleys {
    public static int countingValleys(int steps, String path) {
        int count = 0;
        int a = 0;
        char[] c = path.toCharArray();
      //for(char value : c)
        for(int i = 0; i < c.length; i++){
            switch (c[i]) {
                case 'U' -> {
                    count++;
                    if (count == 0) a++;
                }
                case 'D' -> count--;
            }
        }
        return a;
    }
    public static void main(String[] args) {
        System.out.println(countingValleys(8, "DDUUUUDD"));
    }
}
