package hackerRank.problemSolving;

import java.time.LocalTime;

public class ChallengesKangaroo {
    public static String kangaroo(int x1, int v1, int x2, int v2) {
        String a = "NO";
        int i = 0;
        while (i <= 10000){
            x1 += v1;
            x2 += v2;
            if (x1 == x2) {
                a = "YES";
                break;
            }
            i++;
        }
        return a;
    }

    public static String kangaroo2(int x1, int v1, int x2, int v2) {
        if(v1 != v2 && (x2-x1)/(v1-v2) > 0 && (x2-x1)%(v1-v2) == 0){
            return "YES";
        }
        return "NO";
    }

    public static void main(String[] args) {
//        System.out.println("===== цикл =====");
//        for (int i = 0; i < 1000; i++) {
//            {
//                int a = LocalTime.now().getNano();
//                kangaroo(2564, 5393, 5121, 2836);
//                int b = LocalTime.now().getNano();
//                double h = (double) (b - a) / 1000000000;
//                if (h != 0.0) {
//                    System.out.printf("%f sec\n", h);
//                }
//            }
//        }
        System.out.println("===== if =====");
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        for (int i = 0; i < 1000; i++) {
            {
                int a = LocalTime.now().getNano();
                kangaroo2(2564, 5393, 5121, 2836);
                int b = LocalTime.now().getNano();
                double h = (double) (b - a) / 1000000000;
                if (h != 0) {
                    System.out.printf("%f sec\n", h);
                }
            }
        }
    }
}
