package hackerRank.problemSolving;

import java.util.ArrayList;
import java.util.List;

public class DivisibleSumPairs {

    public static int divisibleSumPairs(int n, int k, List<Integer> ar) {
        int res = 0;
        for (int i = 0; i < ar.size(); i++) {
            for (int j = 0; j < ar.size(); j++) {
                if(i < j){
                    if(ar.get(i) + ar.get(j) == k || (ar.get(i) + ar.get(j)) % k == 0) {
                        System.out.printf("ar[%d] + ar[%d] = %d\n",ar.get(i), ar.get(j), ar.get(i) + ar.get(j));
                        res++;
                    }
                }

            }
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(divisibleSumPairs(6, 3, new ArrayList<>(List.of(1, 3, 2, 6, 1, 2))));
    }
}
