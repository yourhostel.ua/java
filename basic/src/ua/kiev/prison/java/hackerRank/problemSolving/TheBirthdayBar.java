package hackerRank.problemSolving;

import java.util.*;


public class TheBirthdayBar {
    public static int birthday(List<Integer> s, int d, int m) {
        int res = 0;
        for (int i = 0; i <= s.size() - m; i++) {
            int count = 0;
            for (int j = i; j < i + m; j++) count += s.get(j);
            if(count == d) res++;
        }
        return res;
    }
static Integer[] as = new Integer[]{2, 3, 4, 4, 2, 1, 2, 5, 3, 4, 4, 3, 4, 1, 3, 5, 4, 5, 3, 1, 1, 5, 4, 3, 5, 3, 5, 3, 4, 4, 2, 4, 5, 2, 3, 2, 5, 3, 4, 2, 4, 3, 3, 4, 3, 5, 2, 5, 1, 3, 1, 4, 2, 2, 4, 3, 3, 3, 3, 4, 1, 1, 4, 3, 1, 5, 2, 5, 1, 3, 5, 4, 3, 3, 1, 5, 3, 3, 3, 4, 5, 2};
    public static void main(String[] args) {                  //0  1  2  3  4  5
        List<Integer> a = new ArrayList<>(List.of(as));
        System.out.println(birthday(a, 26, 8));
        for(int i = 0; i <= 82 - 8; i++){
            int count = 0;
            for (int j = i; j < i + 8; j++)count += as[j];
            if(count == 26)System.out.printf("%d \n",count);
        }
    }
}