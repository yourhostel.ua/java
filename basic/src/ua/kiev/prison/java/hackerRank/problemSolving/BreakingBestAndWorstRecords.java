package hackerRank.problemSolving;

import java.util.ArrayList;
import java.util.List;

public class BreakingBestAndWorstRecords {
    public static List<Integer> breakingRecords(List<Integer> scores) {
        List<Integer> res = new ArrayList<>();
        int min = scores.get(0), countMin = 0;
        int max = scores.get(0), countMax = 0;
        for (int i = 1; i < scores.size(); i++ ) {
            if (scores.get(i) < min) {
                countMin++;
                min = scores.get(i);
            }
            if (scores.get(i) > max) {
                countMax++;
                max = scores.get(i);
            }
        }
        res.add(countMax);
        res.add(countMin);
        return res;
    }

    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>(List.of(new Integer[]{10, 5, 20, 20, 4, 5, 2, 25, 1}));
        System.out.println(breakingRecords(a));
    }
}
