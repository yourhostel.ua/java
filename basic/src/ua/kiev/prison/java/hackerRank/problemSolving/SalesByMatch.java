package hackerRank.problemSolving;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SalesByMatch {
    public static void main(String[] args) {
        List<Integer> list = List.of(10, 20, 20, 10, 10, 30, 50, 10, 20);
        System.out.println("Total pairs: " + sockMerchant(list.size(), list));
    }

    public static int sockMerchant(int n, List<Integer> ar) {
        Map<Integer, Long> countMap = ar.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        return (int) countMap.values().stream()
                .mapToLong(count -> count / 2)
                .sum();

    }

}
