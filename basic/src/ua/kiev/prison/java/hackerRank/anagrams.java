package hackerRank;

import java.util.Arrays;
import java.util.Scanner;
/**
 * anagram
 * margana
 * Anagrams
 */

/**
 * anagramm
 * marganaa
 * Not Anagrams
 */

/**
 * Hello
 * hello
 * Anagrams
 */

public class anagrams {
    static boolean isAnagram(String a, String b) {
        if(a.length() != b.length())return false;
        char[] l = a.toUpperCase().toCharArray();
        char[] k = b.toUpperCase().toCharArray();
        Arrays.sort(l); Arrays.sort(k);
        for(int i = 0;i<a.length();i++)if (l[i] != k[i]) return false;
        return true;
    }
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        String a = scan.next();
        String b = scan.next();
        scan.close();
        boolean ret = isAnagram(a, b);
        System.out.println( (ret) ? "Anagrams" : "Not Anagrams" );
    }
}
