package hackerRank;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Locale;
import java.util.Scanner;

public class JavaLoops_1 {
    public static void main(String[] args) {
        InputStream in = System.in;
        var scanner = new Scanner(in).useLocale(Locale.US);
        String st = scanner.nextLine();
        int argX = Integer.parseInt(st);


        PrintStream out = System.out;
       for(byte argY = 1; argY <= 10; argY++){
           int result = argX * argY;
           out.printf("%d x %d = %d\n",argX,argY,result);
       }
    }
}
