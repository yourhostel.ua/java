package hackerRank;

import java.util.Scanner;

public class JavaStringReverse {
    public static void main(String[] args) {
        Scanner sc =  new Scanner(System.in);
        String s = sc.nextLine();
        char[] chars = new char[s.length()];
        s.getChars(0, s.length(), chars, 0);
        for(int i = 0, j = s.length() - 1; i <= s.length()/2;) {
            if(chars[i] == chars[j]){
                i++; j--;
            } else {
                System.out.println("No");
                return;
            }
            if(i == s.length()/2 - 1 || s.length() == 1) System.out.println("Yes");
        }
    }
}
