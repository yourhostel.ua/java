package hackerRank;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class intToString {
    public static void main(String[] args) {
        PrintStream out = System.out;
        InputStream in = System.in;
        var scanner = new Scanner(in);
        try{
            int a = scanner.nextInt();
            String b = String.format(String.valueOf(a));
            if(b != null)out.print("Good job");
        }catch (Exception ignored){}
    }
}
