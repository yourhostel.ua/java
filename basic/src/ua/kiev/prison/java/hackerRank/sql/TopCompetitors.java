package hackerRank.sql;

public class TopCompetitors {
    /**
     * https://www.hackerrank.com/challenges/full-score/problem
     */

    String sql = """
            SELECT h.hacker_id, h.name
            FROM HACKERS AS h

            JOIN SUBMISSIONS AS s ON h.hacker_id = s.hacker_id
            JOIN CHALLENGES AS c ON s.challenge_id = c.challenge_id
            JOIN DIFFICULTY AS d ON c.difficulty_level = d.difficulty_level AND s.score = d.score

            GROUP BY h.hacker_id, h.name

            HAVING COUNT(c.challenge_id) > 1
            ORDER BY COUNT(c.challenge_id) DESC, h.hacker_id;
            """;
}
