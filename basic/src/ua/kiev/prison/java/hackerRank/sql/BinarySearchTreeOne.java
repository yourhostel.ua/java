package hackerRank.sql;

public class BinarySearchTreeOne {
    /**
     * <a href="https://www.hackerrank.com/challenges/binary-search-tree-1">Binary Search Tree One</a>
     */

    String sql = """
            SELECT
              N,
              CASE
                -- Якщо родитель відсутній, то це корінь
                WHEN P IS NULL THEN 'Root'
                -- Якщо N не має дітей (не знаходиться у колонці P), то це листок
                WHEN N NOT IN (SELECT P FROM BST WHERE P IS NOT NULL) THEN 'Leaf'
                -- У всіх інших випадках це внутрішній вузол
                ELSE 'Inner'
              END AS NodeType
            FROM BST
            -- Сортуємо вузли за значенням N
            ORDER BY N;
            """;
}
