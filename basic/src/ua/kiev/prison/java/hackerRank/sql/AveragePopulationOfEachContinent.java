package hackerRank.sql;

public class AveragePopulationOfEachContinent {
    /**
     *Given the CITY and COUNTRY tables, query the names of all the continents (COUNTRY.Continent)
     * and their respective average city populations (CITY.Population) rounded down to the nearest integer.
     */
    String sql = """
            SELECT COUNTRY.Continent, FlOOR(AVG(CITY.Population))
            FROM COUNTRY
            JOIN CITY ON CITY.CountryCode=COUNTRY.Code
            GROUP BY COUNTRY.Continent;
            """;
}
