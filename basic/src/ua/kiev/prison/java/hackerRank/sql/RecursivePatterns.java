package hackerRank.sql;

public class RecursivePatterns {
    /**
     * pattern represents P(5)
     *          *
     *          * *
     *          * * *
     *          * * * *
     *          * * * * *
     */
    String sql = """
            WITH RECURSIVE patterns AS (
                SELECT 1 AS level
                UNION ALL
                SELECT level + 1
                FROM patterns
                WHERE level < 20
            )
                        
            SELECT REPEAT('* ', level) AS star_pattern
            FROM patterns;
            """;
}
