package hackerRank.sql;

public class WeatherObservationStation12 {
    /**
     * Query the list of CITY names from STATION that do not start with vowels
     * and do not end with vowels. Your result cannot contain duplicates.
     */
    String sql = """
            SELECT DISTINCT CITY FROM STATION
            WHERE
            CITY NOT REGEXP '^[A|E|I|O|U]'
            AND NOT CITY REGEXP '[A|E|I|O|U]$';
             """;
}
