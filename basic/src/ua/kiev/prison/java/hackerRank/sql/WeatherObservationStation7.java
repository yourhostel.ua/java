package hackerRank.sql;

public class WeatherObservationStation7 {
    /**
     * Query the list of CITY names ending with vowels
     * (i.e., a, e, i, o, or u) from STATION.
     * Your result cannot contain duplicates.
     */
    String sql = """
                    SELECT DISTINCT CITY FROM STATION
                    WHERE
                    CITY REGEXP '[A|E|I|O|U]$';
                    """;
}
