package hackerRank.sql;

public class WhatTypeOfTriangle {
    /**
     *   Table
     *   A   B   C
     *   20  20  23
     *   20  20  20
     *   20  21  22
     *   13  14  30
     *   Sample Output:
     *   Isosceles
     *   Equilateral
     *   Scalene
     *   Not A Triangle
     */
    String sql = """
            SELECT
                CASE
                    WHEN A + B <= C OR A + C <= B OR B + C <= A THEN 'Not A Triangle'
                    WHEN A = B AND B = C THEN 'Equilateral'
                    WHEN A = B OR B = C OR A = C THEN 'Isosceles'
                    ELSE 'Scalene'
                END AS Triangle_Type
            FROM TRIANGLES;
            """;
}
