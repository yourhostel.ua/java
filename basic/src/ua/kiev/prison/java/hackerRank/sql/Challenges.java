package hackerRank.sql;

public class Challenges {
    /**
     * https://www.hackerrank.com/challenges/challenges/problem
     */

    String sql = """
            WITH c AS (
                       SELECT DISTINCT hs.hacker_id, hs.name,
                       COUNT(cs.challenge_id) OVER(PARTITION BY cs.hacker_id) AS nc
                       FROM Hackers hs JOIN Challenges cs ON hs.hacker_id = cs.hacker_id
                      ),
                mc AS (
                       SELECT MAX(nc) AS mx FROM c
                      ),
                 d AS (
                      SELECT nc AS dnc
                      FROM c
                      GROUP BY nc
                      HAVING COUNT(*) > 1
                      )
            SELECT*
            FROM c
            WHERE nc = (SELECT mx FROM mc) OR nc NOT IN (SELECT dnc FROM d)
            ORDER BY nc DESC, hacker_id;
            """;
}
