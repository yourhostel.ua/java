package hackerRank.sql;

public class TheCompany {
    /**
     * <a href="https://www.hackerrank.com/challenges/the-company">The Company</a>
     * Повертає код компанії, ім'я засновника, загальну кількість провідних менеджерів,
     * старших менеджерів, менеджерів та співробітників для кожної компанії.
     * Результат упорядкований за company_code в алфавітному порядку.
     */

    String sql = """
            SELECT
                c.company_code,
                c.founder,
                COUNT(DISTINCT lm.lead_manager_code) AS lead_managers,
                COUNT(DISTINCT sm.senior_manager_code) AS senior_managers,
                COUNT(DISTINCT m.manager_code) AS managers,
                COUNT(DISTINCT e.employee_code) AS employees
            FROM
                company c
            LEFT JOIN
                lead_manager lm ON c.company_code = lm.company_code
            LEFT JOIN
                senior_manager sm ON lm.lead_manager_code = sm.lead_manager_code
            LEFT JOIN
                manager m ON sm.senior_manager_code = m.senior_manager_code
            LEFT JOIN
                employee e ON m.manager_code = e.manager_code
            GROUP BY
                c.company_code, c.founder
            ORDER BY
                c.company_code;
            """;
}
