package hackerRank.sql;

public class WeatherObservationStation5 {
    /**
     * Query the two cities in STATION with the shortest
     * and longest CITY names, as well as their respective lengths
     * (i.e.: number of characters in the name).
     * If there is more than one smallest or largest city,
     * choose the one that comes first when ordered alphabetically.
     */
    String sql = """
    SELECT CITY, LENGTH(CITY)  FROM STATION
    WHERE LENGTH(CITY) = (SELECT MIN(LENGTH(CITY)) FROM STATION)
    ORDER BY CITY
    LIMIT 1;
    SELECT CITY, LENGTH(CITY)  FROM STATION
    WHERE LENGTH(CITY) = (SELECT MAX(LENGTH(CITY)) FROM STATION)
    ORDER BY CITY
    LIMIT 1
    """;
//
//     Sample Output:
//     Amo 3
//     Marine On Saint Croix 21
//
}
