package hackerRank.sql;

public class WeatherObservationStation11 {
    /**
     * Query the list of CITY names from STATION that either do not start with vowels
     * or do not end with vowels. Your result cannot contain duplicates.
     */
    String sql = """
            SELECT DISTINCT CITY FROM STATION
            WHERE
            CITY NOT REGEXP '^[A|E|I|O|U]'
            OR NOT CITY REGEXP '[A|E|I|O|U]$';
             """;
}
