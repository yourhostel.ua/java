package hackerRank.sql;

public class WeatherObservationStation8 {
    /**
     * Query the list of CITY names from STATION which have vowels
     * (i.e., a, e, i, o, and u) as both their first and last characters.
     * Your result cannot contain duplicates.
     */
    String sql = """
                     SELECT DISTINCT CITY FROM STATION
                     WHERE
                     CITY REGEXP '^[A|E|I|O|U]'
                     AND CITY REGEXP '[A|E|I|O|U]$';
            """;
}
