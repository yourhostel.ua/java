package hackerRank.sql;

public class ThePads {
    /**
     *  <a href="https://www.hackerrank.com/challenges/the-pads/problem">hackerrank</a>
     */
    String sql = """
            SELECT CONCAT(Name, '(', SUBSTRING(Occupation, 1, 1), ')')
            FROM OCCUPATIONS
            ORDER BY Name;
            SELECT CONCAT('There are a total of ', COUNT(Occupation), ' ', LOWER(Occupation), 's.')
            FROM OCCUPATIONS
            GROUP BY Occupation
            ORDER BY COUNT(Occupation), Occupation;
            """;
}
