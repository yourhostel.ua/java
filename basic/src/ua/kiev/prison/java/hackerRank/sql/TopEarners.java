package hackerRank.sql;

public class TopEarners {
    /**
     *We define an employee's total earnings to be their monthly (salary x month)  worked,
     * and the maximum total earnings to be the maximum total earnings for any employee in the Employee table.
     * Write a query to find the maximum total earnings for all employees as well as
     * the total number of employees who have maximum total earnings.
     * Then print these values as  space-separated integers.
     */
    String sql = """
            SELECT MAX(MONTHS * SALARY) INTO @monsal FROM EMPLOYEE;

            SELECT @monsal, (SELECT COUNT(MONTHS) FROM EMPLOYEE
                            WHERE MONTHS*SALARY = @monsal);
            """;
}
