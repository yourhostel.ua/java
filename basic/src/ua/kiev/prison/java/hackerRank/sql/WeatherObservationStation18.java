package hackerRank.sql;

public class WeatherObservationStation18 {
    /**
     * Consider P1(a, b) and P1(c, d) to be two points on a 2D plane.
     * a happens to equal the minimum value in Northern Latitude (LAT_N in STATION).
     * b happens to equal the minimum value in Western Longitude (LONG_W in STATION).
     * c happens to equal the maximum value in Northern Latitude (LAT_N in STATION).
     * d happens to equal the maximum value in Western Longitude (LONG_W in STATION).
     * Query the Manhattan Distance between points P1 and P1 and round it to a scale of  decimal places.
     */

    String sql = """
            SELECT MIN(LAT_N) INTO @a FROM STATION;
            SELECT MIN(LONG_W) INTO @b FROM STATION;
            SELECT MAX(LAT_N) INTO @c FROM STATION;
            SELECT MAX(LONG_W) INTO @d FROM STATION;

            SELECT ROUND(ABS(@a-@c)+ABS(@b-@d), 4);
            """;
}
