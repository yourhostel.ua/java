package hackerRank.sql;

public class Occupations {
    /**
     * <a href="https://www.hackerrank.com/challenges/occupations">occupations</a>
     */

    String sql = """
            SET @r0=0, @r1=0, @r2=0, @r3=0;
                        
            SELECT
                MIN(CASE WHEN Occupation = 'Doctor' THEN Name END) AS Doctor,
                MIN(CASE WHEN Occupation = 'Professor' THEN Name END) AS Professor,
                MIN(CASE WHEN Occupation = 'Singer' THEN Name END) AS Singer,
                MIN(CASE WHEN Occupation = 'Actor' THEN Name END) AS Actor
            FROM (
                SELECT
                    Occupation,
                    Name,
                    CASE
                        WHEN Occupation = 'Doctor' THEN @r0:=@r0+1
                        WHEN Occupation = 'Professor' THEN @r1:=@r1+1
                        WHEN Occupation = 'Singer' THEN @r2:=@r2+1
                        WHEN Occupation = 'Actor' THEN @r3:=@r3+1
                    END AS RowNum
                FROM OCCUPATIONS
                ORDER BY Name
            ) Sub
            GROUP BY RowNum
            ORDER BY RowNum;
            """;
}
