package hackerRank.sql;

public class WeatherObservationStation9 {
    /**
     * Query the list of CITY names from STATION that do not start with vowels.
     * Your result cannot contain duplicates.
     */
    String sql = """
                     SELECT DISTINCT CITY FROM STATION
                     WHERE
                     CITY NOT REGEXP '^[A|E|I|O|U]';
            """;
}
