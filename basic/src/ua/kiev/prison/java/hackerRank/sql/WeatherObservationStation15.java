package hackerRank.sql;

public class WeatherObservationStation15 {
    /**
     * Query the Western Longitude (LONG_W) for the largest Northern Latitude (LAT_N) in STATION
     * that is less than 137.2345. Round your answer to  decimal places.
     */

    String sql = """
            SELECT MAX(LAT_N) INTO @myvar FROM STATION
            WHERE LAT_N < 137.2345;
                       
            SELECT ROUND(LONG_W, 4) FROM STATION
            WHERE LAT_N = @myvar
            LIMIT 1;
            """;

    String sql1 = """
            SELECT ROUND(LONG_W, 4) FROM STATION
            WHERE LAT_N = (SELECT MAX(LAT_N) FROM STATION
            WHERE LAT_N < 137.2345)
            LIMIT 1;
            """;
}
