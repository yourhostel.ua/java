package hackerRank.sql;

public class OllivandersInventory {
    /**
     * https://www.hackerrank.com/challenges/harry-potter-and-wands/problem
     */

    String sql = """
            SELECT w1.id, p1.age, w1.coins_needed, w1.power
            FROM Wands w1
            JOIN Wands_Property p1 ON (w1.code = p1.code)
            WHERE p1.is_evil = 0
            AND w1.coins_needed = (SELECT MIN(coins_needed)
            FROM Wands w2
            JOIN Wands_Property p2
            ON (w2.code = p2.code)
            WHERE w2.power = w1.power AND p2.age = p1.age)          
            ORDER BY w1.power DESC, p1.age DESC;
            """;
}
