package hackerRank.sql;

public class TheReport {
    /**
     * https://www.hackerrank.com/challenges/the-report/problem
     */

    String sql = """
            SELECT MYTABLE.Name, MYTABLE.Grade, MYTABLE.Marks
            FROM (SELECT STUDENTS.Name, Grade, STUDENTS.Marks
            FROM GRADES, STUDENTS
            WHERE STUDENTS.Marks >= Min_Mark AND STUDENTS.Marks <= Max_Mark) AS MYTABLE
            WHERE MYTABLE.Grade >= 8
            ORDER BY MYTABLE.Grade DESC, MYTABLE.Name;

            SELECT NULL, MYTABLE.Grade, MYTABLE.Marks
            FROM (SELECT STUDENTS.Name, Grade, STUDENTS.Marks
            FROM GRADES, STUDENTS
            WHERE STUDENTS.Marks >= Min_Mark AND STUDENTS.Marks <= Max_Mark) AS MYTABLE
            WHERE MYTABLE.Grade < 8
            ORDER BY MYTABLE.Grade DESC, MYTABLE.Name;
            """;
}
