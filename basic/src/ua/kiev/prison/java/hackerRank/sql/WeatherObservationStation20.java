package hackerRank.sql;

public class WeatherObservationStation20 {
    /**
     *A median is defined as a number separating the higher half of a data set from the lower half.
     * Query the median of the Northern Latitudes (LAT_N) from STATION and round your answer
     * to 4 decimal places.
     */
    String sql = """
            SELECT FLOOR(COUNT(LAT_N)/2) INTO @myvar FROM STATION;

            SELECT ROUND(
            (SELECT LAT_N FROM (SELECT ROW_NUMBER() OVER (ORDER BY LAT_N) ROW_NUM, LAT_N
            FROM STATION) AS SUB_Q
            WHERE SUB_Q.ROW_NUM = @myvar + 1)
            , 4);
            """;

    String sqlHard = """
            SELECT ROUND(
            (SELECT LAT_N FROM STATION
            ORDER BY LAT_N
            LIMIT 249, 1), 4)
            """;

}
