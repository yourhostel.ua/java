package hackerRank;
import java.math.BigInteger;
import java.util.Scanner;


public class Biginteger {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        do{
            BigInteger x = scanner.nextBigInteger();
            BigInteger y = scanner.nextBigInteger();
            System.out.printf(String.format("%d\n",x.add(y)));
            System.out.printf(String.format("%d", x.multiply(y)));
        }while (scanner.hasNext());
    }
}
