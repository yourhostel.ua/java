package hackerRank;

import java.util.Comparator;
import java.util.Scanner;
import java.util.TreeSet;

public class JavaSort {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] arr = new String[n];
        scanner.nextLine();

        for (int i = 0; i < n; i++) arr[i] = scanner.nextLine();

        String[][] arrD = new String[n][3];

        for (int i = 0; i < n; i++) {
            System.arraycopy(arr[i].split(" "), 0, arrD[i], 0, 3);
        }

//        for (int i = 0; i < n; i++) for (int j = 0; j < 3; j++){
//            System.out.printf("%s ",arrD[i][j]);
//            if(j == 2) System.out.println();
//        }

        System.out.println();

        Comparator<Student> sort =
                new StudentCGPAComparator()
                        .thenComparing(
                                new StudentFirstNameComparator()
                                        .thenComparing(
                                                new StudentIdComparator()
                                        )
                        );
        TreeSet<Student> students = new TreeSet<>(sort);

        for (int i = 0; i < n; i++) {
            students.add(new Student(Integer.parseInt(arrD[i][0]), arrD[i][1], Double.parseDouble(arrD[i][2])));
        }
        for (Student a : students) {
            System.out.println(a.firstName);
        }
    }

    record Student(int id, String firstName, double CGPA) {
    }

    static class StudentCGPAComparator implements Comparator<Student> {
        public int compare(Student a, Student b) {

            return Double.compare(b.CGPA(), a.CGPA());
        }
    }

    static class StudentFirstNameComparator implements Comparator<Student> {
        public int compare(Student a, Student b) {

            return a.firstName().compareTo(b.firstName());
        }
    }

    static class StudentIdComparator implements Comparator<Student> {
        public int compare(Student a, Student b) {

            return Integer.compare(b.id(), a.id());
        }
    }
}