package hackerRank.algorithms;

import java.util.Arrays;
import java.util.List;

public class AVeryBigSum {
    public static long aVeryBigSum(List<Long> ar) {
        return ar.stream().reduce((a,b) -> a + b).get();

    }

    public static void main(String[] args) {
        System.out.println(aVeryBigSum(Arrays.stream(new long[]{256741038, 623958417, 467905213, 714532089, 938071625}).boxed().toList()));
    }
}
