package hackerRank.algorithms;

import java.util.Arrays;
import java.util.List;

public class PermutationEquation {
    public static List<Integer> permutationEquation(List<Integer> p) {
        int[] pe = new int[p.size()];
        for (int i = 0; i < p.size(); i++) {
            pe[i] = p.indexOf(p.indexOf(i + 1) + 1) + 1;
        }
        return Arrays.stream(pe).boxed().toList();
    }

//    List<Integer> pe = new ArrayList<>(p.size());
//        for (int i = 0; i < p.size(); i++) {
//        pe.add(p.indexOf(p.indexOf(i + 1) + 1) + 1);
//    }
//        return pe;

    public static void main(String[] args) {
        System.out.println(permutationEquation(Arrays.stream(new int[]{5, 2, 1, 3, 4}).boxed().toList()));
    }
}
