package hackerRank.algorithms;

public class FindDigits {
    public static int findDigits(int n){
        int count = 0;
        char[] k = String.valueOf(n).toCharArray();
        for (char c : k) {
            int pC = Integer.parseInt(String.valueOf(c));
            if (pC > 0 && n % pC == 0) count++;
        }
       return count;
    }

    public static void main(String[] args) {
        System.out.println(findDigits(124));
    }
}
