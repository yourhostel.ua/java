package hackerRank.algorithms;

import java.util.List;

public class PlusMinus {
    public static void plusMinus(List<Integer> arr) {
        int size = arr.size();
        double [] r = new double[3];
        for (int j : arr) {
            if (j > 0) r[0]++;
            if (j < 0) r[1]++;
            if (j == 0) r[2]++;
        }
        for (double k : r) System.out.printf("%f\n", k/size);

    }

    public static void main(String[] args) {
        plusMinus(List.of(-4, 3, -9, 0, 4, 1));
    }
}
