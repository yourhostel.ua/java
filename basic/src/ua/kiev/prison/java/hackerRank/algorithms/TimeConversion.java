package hackerRank.algorithms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;


public class TimeConversion {
    public static String timeConversion(String s) throws ParseException {
        ZonedDateTime f = new SimpleDateFormat("hh:mm:ssa").parse(s).toInstant().atZone(ZoneId.of("UTC"));
        return f.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }

    public static void main(String[] args) throws ParseException {
        System.out.println(timeConversion("07:05:45PM"));
    }
}
