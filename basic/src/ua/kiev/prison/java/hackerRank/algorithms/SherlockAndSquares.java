package hackerRank.algorithms;

public class SherlockAndSquares {
    public static int squares(int a, int b){
       int res = 0;
       int count = 0;
       for (int j = b ;j >= a; j-- ){
           if(Math.sqrt(j) % 1 == 0) {
               res++;
               count = (int) Math.sqrt(j) - 1;
               break;
           }
       }
       for (int i = 0 ; i <= count; i++) {
           int x = (int) Math.pow(i, 2);
           if(x >= a && x <= b) res++;
       }
       return res;
    }
    public static void main(String[] args) {
        System.out.println(squares(3, 9));
    }
}
