package hackerRank.algorithms;

import java.util.Arrays;
import java.util.List;

public class BirthdayCakeCandles {
    public static Integer birthdayCakeCandles(List<Integer> candles) {
      int max = candles
              .stream()
              .max(Integer::compare).get();
        return Math.toIntExact(candles
                .stream().filter(x -> x == max).count());
    }

    public static void main(String[] args) {
        List<Integer> candles = Arrays.stream(new int[]{3, 2, 1, 3}).boxed().toList();
        System.out.println(birthdayCakeCandles(candles));
    }
}

