package hackerRank.algorithms;

import java.util.Objects;

public class AppendAndDelete {
    public static String appendAndDelete(String s, String t, int k) {
        if (s.length() == 1 && t.length() == 2 && k == t.length()) return "No";//this is weird "y" "yu" "2" = must be YES!

        char[] S = s.toCharArray(), T = t.toCharArray();
        int count = 0;

        for (int i = 0, j = 0; i < S.length && j < T.length; i++, j++) {
            if (S[i] == T[i]) count++;
            if (S[i] != T[i]) break;
        }

        if (S.length >= T.length) {
            if ((S.length - count) * 2 - 1 <= k) return "Yes";
            if (check(count, s, t, S, T)) return "Yes";
        }
            if (check(count, t, s, T, S) && S.length < T.length) return "Yes";
        return "No";
    }

    public static boolean check(int count, String t, String s, char[]T, char[]S) {
            StringBuilder temp = new StringBuilder();
            temp.append(s);
            for (int i = T.length - 1; i >= S.length; i--) temp.append(T[i]);
            return Objects.equals(String.valueOf(temp), t) && count == S.length;
    }

    public static void main(String[] args) {
        System.out.println(appendAndDelete("hackerhappy", "hackerrank", 9));//Yes
        System.out.println(appendAndDelete("zzzzz", "zzzzzzz", 4));//Yes
        System.out.println(appendAndDelete("y", "yu", 2));//No
        System.out.println(appendAndDelete("abcd", "abcdert", 10));//No
        System.out.println(appendAndDelete("ashley", "ash", 2));//No
        System.out.println(appendAndDelete("qwerasdf", "qwerbsdf", 6));//No
        System.out.println(appendAndDelete("aba", "aba", 7));//Yes
        System.out.println(appendAndDelete("aaaaaaaaaa", "aaaaa", 7));//Yes
    }
}
