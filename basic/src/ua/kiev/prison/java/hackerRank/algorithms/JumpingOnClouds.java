package hackerRank.algorithms;

public class JumpingOnClouds {
    public static int jumpingOnClouds(int[] c, int k) {
        int e = 100;
        int l = c.length;
        for (int i = 0;; i = i + k) {
        if (i == l) break;
        if (i > l) i = i - l;
        if (c[i] == 1) e -= 3;
        if (c[i] == 0) e -= 1;
        }
        return e;
    }

    public static void main(String[] args) {
        System.out.println(jumpingOnClouds(new int[]{1, 1, 1, 0, 1, 1, 0, 0, 0, 0}, 3));
    }
}
