package hackerRank.algorithms;

import java.util.List;

public class DiagonalDifference {
    public static int diagonalDifference(List<List<Integer>> arr) {
        int a = 0, b = 0, arrSize = arr.size();

        for(int i = 0, j = 0, k = 0, l = arrSize-1; j < arrSize; i++, l--) {
                a += arr.get(i).get(j);
                b += arr.get(l).get(k);
                j++;
                k++;
            }
        return Math.abs(a - b);

    }

    public static void main(String[] args) {
        List<List<Integer>> arr =
                List.of(new List[]{
                        List.of(new Integer[]{11, 2, 4}),
                        List.of(new Integer[]{4, 5, 6}),
                        List.of(new Integer[]{10, 8, -12})
                });
        System.out.println(diagonalDifference(arr));
    }
}
