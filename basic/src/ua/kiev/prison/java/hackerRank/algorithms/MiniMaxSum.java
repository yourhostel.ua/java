package hackerRank.algorithms;

import java.io.IOException;
import java.util.*;


public class MiniMaxSum {
    public static void result(List<Integer> arr) {
        LinkedList<Integer> max = new LinkedList<>(arr); int i = arr.indexOf(Collections.max(arr));
        LinkedList<Integer> min = new LinkedList<>(arr); int j = arr.indexOf(Collections.min(arr));
        max.remove(i); min.remove(j);
        long xMax = 0L, xMin = 0L;
        for (int x: max)xMax = xMax + x;
        for (int x: min)xMin = xMin + x;

        System.out.printf("%d %d", xMax, xMin);
    }

    public static void main(String[] args) throws IOException {
        List<Integer> list = Arrays.stream(new int[]{256741038, 623958417, 467905213, 714532089, 938071625}).boxed().toList();
        result(list);
    }
}

