package hackerRank;

import java.io.InputStream;
import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class currencyFormatter {
    public static void main(String[] args) {
        PrintStream out = System.out;
        InputStream in = System.in;
        var scanner = new Scanner(in).useLocale(Locale.US);
        Double value = scanner.nextDouble();
        scanner.close();

        out.printf("US: %s\n",NumberFormat.getCurrencyInstance(Locale.US).format(value));
        out.printf("India: %s\n",NumberFormat.getCurrencyInstance(Locale.of("en", "IN")).format(value).replace(String.format("%s",(char)8377), "Rs."));
        out.printf("China: %s\n",NumberFormat.getCurrencyInstance(Locale.CHINA).format(value).replace((char)165, (char)65509));
        out.printf("France: %s\n",NumberFormat.getCurrencyInstance(Locale.FRANCE).format(value));
    }
}
