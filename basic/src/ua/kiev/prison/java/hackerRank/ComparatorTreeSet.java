package hackerRank;

import java.util.*;

public class ComparatorTreeSet {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Comparator<Player> sort = new PlayerScoreComparator()
                .thenComparing(new PlayerNameComparator())
                .thenComparing(new PlayerCountComparator());
        TreeSet<Player> players = new TreeSet<>(sort);
        scanner.nextLine();

        for(int i = 0; i < n; i++){
            String s = scanner.nextLine();
                 if(!s.isEmpty()) {
                     players.add(new Player(s.split(" ")[0], Integer.parseInt(s.split(" ")[1]), i));
                 }
            }

        for (Player a : players) {
            System.out.println(a.name + " " + a.score);
        }
    }

    static class Player {
        String name;
        int score;
        int count;
        public String name() {
            return name;
        }

        public int score() {
            return score;
        }

        public Player(String name, int score, int count) {
            this.name = name;
            this.score = score;
            this.count = count;
        }

        public int count() {
            return count;
        }
    }

    static class PlayerNameComparator implements Comparator<Player> {
        public int compare(Player a, Player b) {

            return a.name().compareTo(b.name());
        }
    }

    static class PlayerScoreComparator implements Comparator<Player> {
        public int compare(Player a, Player b) {

            return Integer.compare(b.score(), a.score());
        }
    }
    static class PlayerCountComparator implements Comparator<Player> {
        public int compare(Player a, Player b) {

            return Integer.compare(a.count(), b.count());
        }
    }
}
