package hackerRank;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Locale;
import java.util.Scanner;

public class DatatypesProblem {
    public static void main(String[] args) {
        PrintStream out = System.out;
        InputStream in = System.in;
        var scanner = new Scanner(in).useLocale(Locale.US);
        String st = scanner.nextLine();
        int k = Integer.parseInt(st);

        String[] arr = new String[k];

        for(byte i = 0; i <= k - 1; i++){
            arr[i] = scanner.nextLine();
            if(i == (k - 1)){

                for (String sample: arr) {
                    String[] arrType = new String[4];
                    try{
                        Byte.parseByte(sample);
                        arrType[0] = "byte";
                    }catch (NumberFormatException ignored){}
                    try{
                        Short.parseShort(sample);
                        arrType[1] = "short";
                    }catch (NumberFormatException ignored){}
                    try{
                        Integer.parseInt(sample);
                        arrType[2] = "int";
                    }catch (NumberFormatException ignored){}
                    try{
                        Long.parseLong(sample);
                        arrType[3] = "long";
                    }catch (NumberFormatException ignored){}
                    if(arrType[0] == null && arrType[1] == null && arrType[2] == null && arrType[3] == null){
                        out.printf("%s can't be fitted anywhere.\n", sample);
                    } else {
                        out.printf("%s can be fitted in:\n", sample);
                        for (String type: arrType){
                            if(type != null) out.printf("* %s\n", type);
                        }
                    }
                }
            }
        }
    }
}
