package hackerRank;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexIPv4 {
    public static void main(String... args) {
        var scanner = new Scanner(System.in);
        Pattern pattern = Pattern.compile("^(((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.){3}+" +
                                                "(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]))$");

        Pattern pattern1 = Pattern.compile("^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
                                                 "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
                                                 "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
                                                 "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");




        while (scanner.hasNext()) {
            Matcher matcher = pattern.matcher(scanner.nextLine());
            //Matcher matcher1 = pattern.matcher(scanner.nextLine());
            System.out.printf("matcher %s\n",matcher.find());
            //System.out.printf("matcher1 %s\n",matcher1.find());
        }
    }
}

//        000.12.12.034
//        121.234.12.12
//        23.45.12.56
//        00.12.123.123123.123
//        122.23
//        Hello.IP
