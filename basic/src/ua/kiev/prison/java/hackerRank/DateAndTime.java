package hackerRank;
import java.time.LocalDate;

public class DateAndTime {
    public static void main(String[] args) {
            LocalDate date = LocalDate.of(1914, 7, 28);
            System.out.printf("%s", date.getDayOfWeek());
    }
}
