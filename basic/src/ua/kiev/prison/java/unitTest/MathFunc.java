package unitTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import static org.junit.Assert.assertEquals;


public class MathFunc {
    public static void main(String[] args) {

    }
    int calls;

    public int getCalls() {
        return calls;
    }

    public long factorial(int number) {
        calls++;

        if (number < 0)
            throw new IllegalArgumentException();

        long result = 1;
        if (number > 1) {
            for (int i = 1; i <= number; i++)
                result = result * i;
        }

        return result;
    }

    public long plus(int num1, int num2) {
        calls++;
        return num1 + num2;
    }
    public static class MathFuncTest {
        private MathFunc math;

        @Before
        public void init() { math = new MathFunc(); }
        @After
        public void tearDown() { math = null; }

        @Test
        public void calls() {
            assertEquals(0, math.getCalls());

            math.factorial(1);
            assertEquals(1, math.getCalls());

            math.factorial(1);
            assertEquals(2, math.getCalls());
        }

        @Test
        public void factorial() {
            assertEquals(1, math.factorial(0));
            assertEquals(1, math.factorial(1));
            assertEquals(120, math.factorial(5));
        }

        @Test(expected = IllegalArgumentException.class)
        public void factorialNegative() {
            math.factorial(-1);
        }

        @Ignore
        @Test
        public void todo() {
            assertEquals(3, math.plus(1, 1));
        }
        public static void main(String[] args) throws Exception {
            JUnitCore runner = new JUnitCore();
            Result result = runner.run(MathFuncTest.class);
            System.out.println("run tests: " + result.getRunCount());
            System.out.println("failed tests: " + result.getFailureCount());
            System.out.println("ignored tests: " + result.getIgnoreCount());
            System.out.println("success: " + result.wasSuccessful());
        }
    }
}

