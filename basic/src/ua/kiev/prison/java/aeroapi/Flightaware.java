package aeroapi;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static libs.Console.log;

public class Flightaware {
    public static void main(String[] args) {
        try {
            getAero();
        } catch (IOException | InterruptedException e) {
            log("Неудачно");
        }
    }
    public static void getAero() throws IOException, InterruptedException {
        String YOUR_API_KEY = "HFVb5dbEv0ImAa9C72jjrT0nts519bJA";
        String apiUrl = "https://aeroapi.flightaware.com/aeroapi/";

        String airport = "SKN";

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(apiUrl + "airports/" + airport + "/flights"))
                .headers("x-apikey", YOUR_API_KEY)
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        if (response.statusCode() == 200) {
            System.out.println("responseBody: " + response.body());
        }
    }
}
