package date.test;
import org.junit.Assert;
import org.junit.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class ExTest {
    @Test
    public void testZoneId() throws Exception {
        // case-1
        ZoneId zid1 = ZoneId.of("Europe/Moscow");
        Assert.assertEquals("ZoneRegion", zid1.getClass().getSimpleName());

        // case-2
        ZoneId zid2 = ZoneId.of("UTC+4");
        Assert.assertEquals("ZoneRegion", zid2.getClass().getSimpleName());

        // case-3
        ZoneId zid3 = ZoneId.of("+03:00:00");
        Assert.assertEquals("ZoneOffset", zid3.getClass().getSimpleName());

// Не очень понятно почему case-4, который фактически запрашивает тоже что и case-3,
// в результате создает java.time.ZoneRegion, а не java.time.ZoneOffset.

        // case-4
        ZoneId zid4 = ZoneId.ofOffset("UTC", ZoneOffset.of("+03:00:00"));
        Assert.assertEquals("ZoneRegion", zid4.getClass().getSimpleName());
    }

    @Test
    public void testZoneUTC() throws Exception {
        ZoneId zid1 = ZoneOffset.UTC;
        Assert.assertEquals("ZoneOffset", zid1.getClass().getSimpleName());

        ZoneId zid2 = ZoneId.of("Z");
        Assert.assertEquals("ZoneOffset", zid2.getClass().getSimpleName());
        Assert.assertSame(ZoneOffset.UTC, zid2);

        //Для временной зоны UTC заведена специальная константа java.time.ZoneOffset#UTC,
        // тем не менее запрос на ZoneId.of(«UTC») в новом API выдает уже объект класса java.util.ZoneRegion,
        // а не эту константу.
        ZoneId zid3 = ZoneId.of("UTC");
        Assert.assertEquals("ZoneRegion", zid3.getClass().getSimpleName());
    }
//java.time.Instant — это новый java.util.Date, только неизменяемый,
// с наносекундной точностью и корректным названием.
// Внутри хранит Unix-time в виде двух полей: long с количеством секунд,
// и int с количеством наносекунд внутри текущей секунды.
//
//Значение обоих полей можно запросить напрямую, а также можно попросить посчитать более привычное
// для старого API представление Unix-time в виде миллисекунд:
    @Test
    public void testInstantFields() throws Exception {
        Instant instant = Clock.systemDefaultZone().instant();

        System.out.println(instant.getEpochSecond());
        System.out.println(instant.getNano());

        System.out.println(instant.toEpochMilli());
    }

    /**
    *Также как и java.util.Date (при правильном его использовании),
     * объект класса java.time.Instant ничего не знает про временную зону.
     * Отдельно стоит сказать про метод java.time.Instant.toString().
     * Если раньше java.util.Date.toString() работал с учетом текущей локали и временной зоны по умолчанию,
     * то новый java.time.Instant.toString() всегда формирует текстовое представление во временной зоне
     * UTC и одинаковым форматом ISO-8601 — это касается и вывода переменных в IDE при отладке:
     */

    @Test
    public void testInstantString() throws Exception {
        Instant instant1 = Clock.system(ZoneId.of("Europe/Paris")).instant();
        System.out.println(instant1.toString());

        Instant instant2 = Clock.systemUTC().instant();
        System.out.println(instant2.toString());

        Instant instant3 = Clock.systemDefaultZone().instant();
        System.out.println(instant3.toString());
    }
}
