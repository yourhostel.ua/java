package date;

import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;


public class Date {
    public static void main(String[] args) {
        System.out.println(LocalDate.now());
        System.out.println(LocalTime.now());
        System.out.println(LocalDateTime.now());
        System.out.println(LocalDate.of(2020, Month.SEPTEMBER, 23));
        System.out.println(LocalDate.of(2021, 1, 1));
        System.out.println(LocalDate.ofYearDay(2020, 361));
        System.out.println(LocalTime.of(12, 10));
        System.out.println(LocalTime.of(12, 10));
        System.out.println(LocalTime.of(18, 15, 10));
        System.out.println(LocalTime.of(23, 59, 59, 700));//23:59:59.000000700
        System.out.println(LocalTime.ofSecondOfDay(9_124));
        System.out.println(LocalTime.ofNanoOfDay(100_000_000_000L));
        System.out.println("----------------------------------------");
        String[] b = ("2020/11/28").split("/");
        int y = Integer.parseInt(b[0]);
        int m = Integer.parseInt(b[1]);
        int d = Integer.parseInt(b[2]);

        LocalDate ld = LocalDate.of(y,m,d);
        long ts = Timestamp.valueOf(LocalDate.of(y,m,d).atStartOfDay()).getTime();

        DateTimeFormatter g = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        System.out.println(ld.format(g));
        System.out.println(LocalDate.now().getYear() - ld.getYear());
        System.out.println(ts);
        System.out.println(LocalDate.now().minusYears(2).toEpochDay());
        System.out.println("----------------------------------------");
        LocalDate now = LocalDate.now();
        long _2017 = Timestamp.valueOf(LocalDate.from(LocalDate.of(2017, 9, 23).atTime(10,52)).atStartOfDay()).getTime();
//        System.out.println(now.isAfter(_2017));// true
//        System.out.println(now.isBefore(_2017));// false

    }
}
