package enumClass;

import java.util.EnumSet;
import java.util.Set;

public class ConstructorAndMethod {
    public static void main(String[] args) {
        System.out.println(Color.RED.getCode());        // #FF0000
        System.out.println(Color.GREEN.getCode());      // #00FF00
        Set<Color> colors = EnumSet.range(Color.RED,Color.BLUE);
        Set<Color> colors2 = EnumSet.allOf(Color.class);
        System.out.println(colors); //output: [RED, BLUE]
        System.out.println(colors2); //output: [RED, BLUE, GREEN]
    }
}

enum Color{
    RED("#FF0000"), BLUE("#0000FF"), GREEN("#00FF00");
    private final String code;
    Color(String code){
        this.code = code;
    } //only default private
    public String getCode(){ return code;}
}
