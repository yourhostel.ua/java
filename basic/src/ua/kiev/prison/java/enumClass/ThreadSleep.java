package enumClass;

public class ThreadSleep {
    public static void main(String[] args) {
        Day[] days = Day.values();
        for(int i = 0; i < 7; i++) {
            System.out.println(days[i]);
            try {
                Thread.sleep(1000);
            } catch(InterruptedException ignored) {}
        }

        Day current = Day.DEFAULT;
        System.out.println(current); // DEFAULT
        System.out.println(Day.MONDAY.ordinal());// 0 перечисление начинаеться с 0
    }
    enum Day{ //enum from enumeration
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY,
        DEFAULT
    }
}
