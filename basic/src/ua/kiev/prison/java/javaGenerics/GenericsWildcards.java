package javaGenerics;

import java.util.ArrayList;
import java.util.List;

public class GenericsWildcards {
    private static void printList(List<?> list) {
        System.out.println("Начало нового списка: ");

        for (Object l : list) {
            System.out.println("{" + l + "}");
        }
    }
    public static void main(String[] args) {
        List<Integer> intList = new ArrayList<>();
        intList.add(7);
        intList.add(777);
        printList(intList);

        List<String> strList = new ArrayList<>();
        strList.add("7");
        strList.add("любая строка");
        strList.add("777");
        printList(strList);
    }
}
