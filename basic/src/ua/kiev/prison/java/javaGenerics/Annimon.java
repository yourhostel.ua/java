package javaGenerics;

public class Annimon {
    public static void main(String[] args) {
//        Garage garage = new Garage();
//        garage.set(new Car("Aston Martin"));

        Garage<Car> garage = new Garage<>();
        garage.set(new Car("Aston Martin"));
        Car car = garage.get();
        System.out.println(car.getName()); // Aston Martin

//        Garage garage2 = new Garage();
//        garage2.set(new Motorcycle("Honda CBR500R"));

        Garage<Motorcycle> garage2 = new Garage<>();
        garage2.set(new Motorcycle("Honda CBR500R"));
        Motorcycle motorcycle = garage2.get(); //убрав двойку при явной проверке типа будет теперь ошибка
        System.out.println(motorcycle.getName());

//        class Jupiter {
//        }

//        Garage<Jupiter> garage3 = new Garage<>(); //ошибка должен экстендится от Vehicle
//        garage3.set(new Jupiter());
//        Jupiter jupiter = garage3.get();
    }

    public abstract static class Vehicle {

        protected final String name;

        public Vehicle(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static class Car extends Vehicle {

        public Car(String name) {
            super(name);
        }
    }

    public static class Motorcycle extends Vehicle {

        public Motorcycle(String name) {
            super(name);
        }
    }

    public static class Garage<T extends Vehicle> {

        private T vehicle;// T все равно должно быть в поле несмотря на extends Vehicle

        public T get() {
            return vehicle;
        }

        public void set(T vehicle) {
            this.vehicle = vehicle;
        }
    }
}
