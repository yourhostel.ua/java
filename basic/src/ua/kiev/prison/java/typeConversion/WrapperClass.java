package typeConversion;

/*
 *       Тип	 Класс-обертка
 * ------------------------------
 *       byte	    Byte
 *       short	    Short
 *       int	    Integer
 *       long	    Long
 *       char	    Character
 *       float	    Float
 *       double	    Double
 *       boolean	Boolean
 */

public class WrapperClass {

    static Integer in = 5;
    static Float fl = 2.0f;
    static Short sh = 5;
    static Double db = 5.4d;
    static String st = "5";

    public static void main(String[] args) {

        Number numA = fl;
        System.out.println("fl = 2.0f => numA: " + numA);

        Number numB = new Double(3.8);
        System.out.println("new Double(3.8) => numB: " + numB);

        Integer inA = numA.intValue();
        Integer inB = numB.intValue();
        System.out.println("inA = " + inA + "; inB = " + inB);
    }

}


