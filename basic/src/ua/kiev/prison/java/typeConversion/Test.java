package typeConversion;

public class Test {
    static long b = 15; //64 бит	          от -9223372036854775808L до 9223372036854775807L
    static int a = 0; //32 бит	          от -2147483648 до 2147483647
    static char d = 'd';

    public static void main(String[] args) {
        int e = d; // перетворює на 100 Unicode Decimal https://www.tamasoft.co.jp/en/general-info/unicode-decimal.html

        System.out.println("e = " + e);

        a = (int) b; //з великої коробки у маленьку тому що маленької достатньо для вмісту великої

        System.out.println("a = " + a);

        double n = 128;// не влізе в byte

        for (byte i = 0; i <= 10; i++){
            n++;
            byte m = (byte) n; //byte от -128 до 127
            System.out.println("m = " + m); //у консолі побачимо: m = -127, m = -126...
        }
    }
}
