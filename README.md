# Practice by Java

created 24/03/2023

## Basic

- [ ] Java-21
- [String Templates](https://gitlab.com/yourhostel.ua/java/-/tree/main/basic/src/ua/kiev/prison/java/java_21/StringTemplates.java)
- [SequencedCollection](https://gitlab.com/yourhostel.ua/java/-/tree/main/basic/src/ua/kiev/prison/java/java_21/SequencedCollectionExample.java)
- [Virtual thread](https://gitlab.com/yourhostel.ua/java/-/tree/main/basic/src/ua/kiev/prison/java/java_21/VirtualThreadExample.java)
- [Foreign Function](https://gitlab.com/yourhostel.ua/java/-/tree/main/basic/src/ua/kiev/prison/java/java_21/foreign_function/DLLTest.java)
- [Vector Api](https://gitlab.com/yourhostel.ua/java/-/tree/main/basic/src/ua/kiev/prison/java/java_21/vector_api/MatrixMultiplicationBenchmark.java)
      
#### HackerRank

- [ ] Java

- [Input output](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/inputOutput.java)
- [Output Formatting](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/OutputFormatting.java)
- [Java loops_1](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/JavaLoops_1.java)
- [Data types problem](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/DatatypesProblem.java)
- [Has next](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/HasNext.java)
- [Java loops_2](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/JavaLoops_2.java)
- [Int to string](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/intToString.java)
- [Java string compare](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/JavaStringCompare.java)
- [Java string reverse](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/JavaStringReverse.java)
- [Java strings introduction](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/JavaStringsIntroduction.java)
- [Static initializer exception](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/StaticInitializerException.java)
- [Currency formatter](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/currencyFormatter.java)
- [String tokens](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/StringTokens.java)
- [Pattern syntax](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/PatternSyntax.java)
- [Username checker](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/UsernameChecker.java)
- [Pattern compile](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/PatternCompile.java)
- [Anagrams](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/anagrams.java)
- [Date and time](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/DateAndTime.java)
- [Regex IP v.4](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/RegexIPv4.java)
- [Biginteger](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/Biginteger.java)
- [Array introduction](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/ArrayIntroduction.java)
- [Prime](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/Prime.java)
- [Array 2d](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/Array2d.java)
- [Negative sub array](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/NegativeSubArray.java)
- [Method overriding](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/MethodOverriding.java)
- [Array list](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/Arraylist.java)
- [Overloading method](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/GenericMethods.java)
- [Java sort(Comparator TreeSet 1)](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/JavaSort.java)
- [Comparator TreeSet 2](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/ComparatorTreeSet.java)

- [ ] Problem Solving

- [Breaking best and worst records](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/problemSolving/BreakingBestAndWorstRecords.java)
- [Apples and oranges](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/problemSolving/ApplesAndOranges.java)
- [Counting valleys](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/problemSolving/CountingValleys.java)
- [Challenges Kangaroo](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/problemSolving/ChallengesKangaroo.java)
- [Between тwo sets](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/problemSolving/BetweenTwoSets.java)
- [The birthday bar](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/problemSolving/TheBirthdayBar.java)
- [Divisible sum pairs](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/problemSolving/DivisibleSumPairs.java)
- [Alice and Bob](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/problemSolving/AliceAndBob.java)
- [Divisible sum pairs](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/problemSolving/DivisibleSumPairs.java)
- [Sales by match](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/problemSolving/SalesByMatch.java)
 
- [ ] Algorithms

- [Mini max sum](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/algorithms/MiniMaxSum.java)
- [Stair case](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/algorithms/Staircase.java)
- [Birthday cake Candles](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/algorithms/BirthdayCakeCandles.java)
- [Time conversion](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/algorithms/TimeConversion.java)
- [Permutation equation](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/algorithms/PermutationEquation.java)
- [Jumping on clouds](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/algorithms/JumpingOnClouds.java)
- [Extra long factorials](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/algorithms/ExtraLongFactorials.java)
- [Find digits](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/algorithms/FindDigits.java)
- [Append and delete](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/algorithms/AppendAndDelete.java)
- [Sherlock and squares](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/algorithms/SherlockAndSquares.java)
- [A Very Big Sum](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/algorithms/AVeryBigSum.java)
- [Diagonal Difference](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/algorithms/DiagonalDifference.java)
- [Plus Minus](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/algorithms/PlusMinus.java)

- [ ] SQL
- [Weather observation station 2](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation2.java)
- [Weather observation station 5](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation5.java)
- [Weather observation station 6](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation6.java)
- [Weather observation station 7](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation7.java)
- [Weather observation station 8](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation8.java)
- [Weather observation station 9](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation9.java)
- [Weather observation station 10](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation10.java)
- [Weather observation station 11](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation11.java)
- [Weather observation station 12](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation12.java)
- [Weather observation station 13](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation13.java)
- [Weather observation station 14](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation14.java)
- [Weather observation station 15](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation15.java)
- [Weather observation station 18](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation18.java)
- [Weather observation station 19](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation19.java)
- [Weather observation station 20](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WeatherObservationStation20.java)
- [Higher Than 75 Marks](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/HigherThan75Marks.java)
- [Employee Names](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/EmployeeNames.java)
- [Top Earners](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/TopEarners.java)
- [Population Census](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/PopulationCensus.java)
- [Employee Salaries](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/EmployeeSalaries.java)
- [Average Population of Each Continent](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/AveragePopulationOfEachContinent.java)
- [The Report](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/TheReport.java)
- [Top Competitors](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/TopCompetitors.java)
- [Ollivanders Inventory](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/OllivandersInventory.java)
- [Challenges](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/Challenges.java)
- [Recursive patterns](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/RecursivePatterns.java)
- [What type of triangle](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/WhatTypeOfTriangle.java)
- [The pads](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/ThePads.java)
- [The Company](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/hackerRank/sql/TheCompany.java)

#### Types

- [Boolean type](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/type/BooleanType.java)
- [Characters](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/type/Characters.java)
- [Floating point types](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/type/FloatingPointTypes.java)
- [Integral types](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/type/IntegralTypes.java)
- [Strings](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/type/Strings.java)

most of the material from [here](https://ru.wikibooks.org/wiki/Java/Типы_данных)

#### Type Conversion

- [Test](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/typeConversion/Test.java)
- [Wrapper class](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/typeConversion/WrapperClass.java)
#### Scanner
- [Three lines input](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/scanner/ThreeLinesInput.java)
#### Operators
- [Arithmetic](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/operators/Arithmetic.java)
- [Comparison](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/operators/Comparison.java)
- [Bitwise](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/operators/Bitwise.java)
#### Conditional statements
- [If else](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/conditionalStatements/IfElse.java)

#### Classes, objects and arrays

- [Enum class](https://gitlab.com/yourhostel.ua/java/-/tree/main/basic/src/ua/kiev/prison/java/enumClass)
- [Array](https://gitlab.com/yourhostel.ua/java/-/tree/main/basic/src/ua/kiev/prison/java/array)

#### Java Generics

- [Basic example](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/JavaGenerics/Annimon.java)
- [Generics Wild cards](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/JavaGenerics/GenericsWildcards.java)

#### Date

- [Base](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/date/Date.java)
- [Example ZonedDateTime](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/date/ExampleZonedDateTime.java)
- [ExTest](https://gitlab.com/yourhostel.ua/java/-/blob/main/basic/src/ua/kiev/prison/java/date/test/ExTest.java)